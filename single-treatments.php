<?php get_header(); ?>
<main class="single-treatment">

<?php while ( have_posts() ) :
the_post(); ?>

<!-- Sekcja nagłówka -->
<?php the_title('<h1 class="page-title">', '</h1>'); ?>

<nav class="single-top-navigation">
	<a href="javascript:history.back()">&nbsp;</a> Powrót do wyboru zabiegu
</nav>

<!-- Opis produktu -->
<section class="treatment-intro">
	<div class="row">
		<div class="description col-lg-4">
			<?php the_field('treatment-description'); ?>
		</div>
		<div class="image col-lg-8 parallax" style="background: url('<?php echo get_field('treatment-background'); ?>')">
		</div>
	</div>
</section>

<div class="content">
	<?php the_content(); ?>
</div>



<?php endwhile; ?>

</main>
<?php get_footer();