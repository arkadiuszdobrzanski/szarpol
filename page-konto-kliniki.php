<?php get_header(); ?>
	<main class="main-clinic-account">
		<?php the_title('<h1 class="page-title">', '</h1>'); ?>
		<?php
			if ( is_user_logged_in() ): ?>
				
				<section class="row clinic-section">
						<aside lass="col-md-3">
							<h2>Zalogowano</h2>
							<div class="row">
								<div class="col-big">
								<?php echo  wp_get_current_user()->user_firstname . ' ' . wp_get_current_user()->user_lastname; ?><br>
								<?php the_field('nazwa_kliniki', 'user_'. get_current_user_id()); ?><br><br>
								</div>
								<div class="col-small">
									<a href="<?php echo wp_logout_url(home_url()); ?>" class="button-outline">Wyloguj się</a>
								</div>
							</div>
							<h2>Menu</h2>
							<ul>
								<li <?php echo (empty($_GET['typ']) ? 'class="active"' : '') ?>><a href="<?php echo esc_url(remove_query_arg('typ')); ?>">Posiadane produkty</a></li>
								<li <?php echo (($_GET['typ'] == 'materialy-marketingowe') ? 'class="active"' : '' ); ?>><a href="<?php echo esc_url(add_query_arg('typ', 'materialy-marketingowe')); ?> ">Materiały marketingowe</a></li>
						</aside>

					<div class="col-md-6">
						<div class="appliances">
						<?php if (!empty($_GET['typ'])):

								$post = get_post(694); 
								$content = apply_filters('the_content', $post->post_content); 
								echo $content.'<br>';

								if( have_rows('files', 694) ): ?>
									<ul class="pliki-do-pobrania">
									<?php while ( have_rows('files', 694) ) : the_row();

												if( get_row_layout() == 'pliki_do_pobrania_ze_strony' ): ?>

													<li><a href="<?php echo get_sub_field('plik')['url'] ?>" target="_blank"><?php echo get_sub_field('plik')['title']; ?></a></li>

												<?php elseif( get_row_layout() == 'zrodlo_zewnetrzne' ): ?>

													<li><a href="<?php echo get_sub_field('link')['url']; ?>" target="<?php echo (get_sub_field('link')['target'] ? get_sub_field('link')['target'] : '_self'); ?>"><?php echo get_sub_field('link')['title']; ?></a></li>

												<?php endif;
											endwhile; ?>
											</ul>
										<?php else : ?>

										<p>Brak materiałów do pobrania.</p>

										<?php endif;
									else:
										$appliances = get_field('urzadzenia', 'user_'. get_current_user_id());

								if( $appliances ): ?>
									<?php foreach( $appliances as $post): ?>
										<?php setup_postdata($post); ?>
										<div class="row">
											<div class="col-xl-3 col-lg-4 col-md-5">
												<a href="<?php the_permalink(); ?>">
												<?php the_post_thumbnail('thumbnail'); ?>
												</a>
											</div>
											<div class="col-xl-9 col-lg-8 col-md-7 col-sm-12">
												<a href="<?php the_permalink(); ?>"><?php the_title('<h2>', '</h2>'); ?></a>
												<?php
												if( have_rows('files') ): ?>
													<ul class="pliki-do-pobrania">
													<?php while ( have_rows('files') ) : the_row();

														if( get_row_layout() == 'pliki_do_pobrania_ze_strony' ): ?>

															<li><a href="<?php echo get_sub_field('plik')['url'] ?>" target="_blank"><?php echo get_sub_field('plik')['title']; ?></a></li>

														<?php elseif( get_row_layout() == 'zrodlo_zewnetrzne' ): ?>

															<li><a href="<?php echo get_sub_field('link')['url']; ?>" target="<?php echo (get_sub_field('link')['target'] ? get_sub_field('link')['target'] : '_self'); ?>"><?php echo get_sub_field('link')['title']; ?></a></li>

														<?php endif;
													endwhile; ?>
													</ul>
												<?php else : ?>

												<p>Brak materiałów do pobrania.</p>

												<?php endif; ?>
											</div>
												</div>
									<?php endforeach; ?>
									<?php wp_reset_postdata(); ?>
								<?php endif;
							endif;
						?>
						</div>
					</div>
					<div class="col-md-3">
						<div class="call-us">
							<img src="<?php echo get_template_directory_uri(); ?>/img/konsultant.png">
							<p>Masz pytania? Porozmawiaj z konsultantem.</p>
							<a href="<?php the_permalink(get_page_by_path( 'kontakt' )); ?>" class="button-outline">Kontakt</a>
						</div>
					</div>
				</section>

			<?php else: ?>
					<section class="row login-section"> 
						<div class="col-sm-6">
							<div class="form-wrapper">
								<h2>Zaloguj się</h2>
								<?php 
								 $args = array(
									'echo' => true,
									// This could be your User's Current Page?
									'redirect' => get_permalink(681),
									'form_id' => 'user-login-form',
									'label_username' => __( 'Username' ),
									'label_password' => __( 'Password' ),
									'label_remember' => __( 'Remember Me' ),
									'label_log_in' => __( 'Log In' ),
									'id_username' => 'user_login',
									'id_password' => 'user_pass',
									'id_remember' => 'rememberme',
									'id_submit' => 'wp-submit',
									'remember' => true,
									'value_username' => NULL,
									'value_remember' => false
								);
								wp_login_form($args); 
								 ?>
							</div>
						</div>
						<div class="col-sm-6">
						</div>
					</section>
			<?php endif;
		?>
	</main>

<?php get_footer();