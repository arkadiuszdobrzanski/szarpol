<?php
/**
 * Twenty Nineteen functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */


add_action( 'rest_api_init', function () {
  register_rest_route( 'mapka/v1', '/getMapObject/(?P<slug>[^.]+)', array(
    'methods' => 'GET',
    'callback' => 'getMapObject',
  ) );
} );
function render($template, Array $data) {

	extract($data);
	ob_start();

	include( $template);
	$content = ob_get_contents();
	ob_end_clean();

	return $content;
}

//paginacja na aktualnosciach
add_action( 'rest_api_init', function () {
  register_rest_route( 'paginacja/v1', '/getNews/(?P<strona>[^.]+)', array(
    'methods' => 'GET',
    'callback' => 'getNews',
  ) );
} );

function getNews($data) {
    $args = array(
        'post_type' => 'aktualnosci',
        'post_status' => 'publish',
        'posts_per_page' => 8,
        'paged'=>$data['strona']
    );
    $loop = new WP_Query( $args );
    return render('template-parts/paginacja.php',$loop->posts);
}



//nowa wyszukiwarka
add_action( 'rest_api_init', function () {
    register_rest_route( 'paginacja/v1', '/getProductsSearch/(?P<strona>[^.]+)/(?P<keyword>[^.]+)', array(
        'methods' => 'GET',
        'callback' => 'getProductsSearch',
    ) );
} );

add_action( 'rest_api_init', function () {
    register_rest_route( 'paginacja/v1', '/getBlogSearch/(?P<strona>[^.]+)/(?P<keyword>[^.]+)', array(
        'methods' => 'GET',
        'callback' => 'getBlogSearch',
    ) );
} );

add_action( 'rest_api_init', function () {
    register_rest_route( 'paginacja/v1', '/getNewsSearch/(?P<strona>[^.]+)/(?P<keyword>[^.]+)', array(
        'methods' => 'GET',
        'callback' => 'getNewsSearch',
    ) );
} );

function getProductsSearch($data) {
    $args = array(
        'post_type' => 'appliance',
        'post_status' => 'publish',
        'posts_per_page' => 8,
        's' => $data['keyword'],
        'paged'=>$data['strona']
    );
    $loop = new WP_Query( $args );
    return render('template-parts/paginacja-appliance.php',$loop->posts);
}

function getBlogSearch($data) {
    $args = array(
        'post_type' => 'blog',
        'post_status' => 'publish',
        'posts_per_page' => 6,
        's' => $data['keyword'],
        'paged'=>$data['strona']
    );
    $loop = new WP_Query( $args );
    return render('template-parts/paginacja-blog.php',$loop->posts);
}

function getNewsSearch($data) {
    $args = array(
        'post_type' => 'aktualnosci',
        'post_status' => 'publish',
        'posts_per_page' => 6,
        's' => $data['keyword'],
        'paged'=>$data['strona']
    );
    $loop = new WP_Query( $args );
    return render('template-parts/paginacja-news.php',$loop->posts);
}



function getMapObject($data) {
	$loop = new WP_Query(array(
		'post_type' => 'przedstawiciele',
		'ignore_sticky_posts' => 1,
		'posts_per_page' => -1,
		'post_status' => array('publish'),
		'meta_query' => array(
				array(
						'key'     => 'wojewodztwo',
						'value'   => '"'.$data['slug'].'"',
						'compare' => 'LIKE',
				),
		),

));

return render('template-parts/map.php',$loop->posts);
}

add_action('init', 'my_pagination_rewrite');
require get_template_directory().'/inc/cleanup.php';
require get_template_directory().'/inc/support.php';
require get_template_directory().'/inc/breadcrumb.php';
require get_template_directory().'/inc/widgets.php';
require get_template_directory().'/inc/enqueue.php';
require get_template_directory().'/inc/custom-posts.php';

// Gutenberg blocks
require get_template_directory().'/inc/blocks.php';


/**
 * Twenty Nineteen only works in WordPress 4.7 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '5.0', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
	return;
}

if ( ! function_exists( 'frontendhouse_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function frontendhouse_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Twenty Nineteen, use a find and replace
		 * to change 'twentynineteen' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'twentynineteen', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );
		set_post_thumbnail_size( 1568, 9999 );

		// This theme uses wp_nav_menu() in two locations.
		register_nav_menus(
			array(
				'footer' => __( 'Footer Menu', 'frontendhouse' ),
				'social' => __( 'Social Links Menu', 'frontendhouse' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
			)
		);

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 190,
				'width'       => 190,
				'flex-width'  => false,
				'flex-height' => false,
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		// Add support for Block Styles.
		add_theme_support( 'wp-block-styles' );

		// Add support for full and wide align images.
		add_theme_support( 'align-wide' );


		// Add support for editor styles.
		add_theme_support( 'editor-styles' );

		// Enqueue editor styles.
		add_editor_style( 'style-editor.css' );



		acf_add_options_page(array(
			'page_title'    => 'Shar-pol - ustawienia templatki',
			'menu_title'    => 'Ustawienia templatki',
			'menu_slug'     => 'sharpol-theme-general-settings',
			'capability'    => 'edit_posts',
			'redirect'      => false,
			'icon_url' => 'https://shar-pol.pl/wp-content/uploads/2021/02/cropped-favicon1-32x32.png',
	));

	acf_add_options_sub_page(array(
		'page_title'    => 'Ustawienia stopki',
		'menu_title'    => 'Stopka',
		'parent_slug'   => 'sharpol-theme-general-settings',
));

		// Add custom editor font sizes.
		add_theme_support(
			'editor-font-sizes',
			array(
				array(
					'name'      => __( 'Small', 'twentynineteen' ),
					'shortName' => __( 'S', 'twentynineteen' ),
					'size'      => 19.5,
					'slug'      => 'small',
				),
				array(
					'name'      => __( 'Normal', 'twentynineteen' ),
					'shortName' => __( 'M', 'twentynineteen' ),
					'size'      => 22,
					'slug'      => 'normal',
				),
				array(
					'name'      => __( 'Large', 'twentynineteen' ),
					'shortName' => __( 'L', 'twentynineteen' ),
					'size'      => 36.5,
					'slug'      => 'large',
				),
				array(
					'name'      => __( 'Huge', 'twentynineteen' ),
					'shortName' => __( 'XL', 'twentynineteen' ),
					'size'      => 49.5,
					'slug'      => 'huge',
				),
			)
		);

		// Editor color palette.
		add_theme_support(
			'editor-color-palette',
			array(
				array(
					'name'  => __( 'Błękitny', 'twentynineteen' ),
					'slug'  => 'pale-blue',
					'color' => '#E7F4FB',
				),
				array(
					'name'  => __( 'Turkusowy', 'twentynineteen' ),
					'slug'  => 'pale-turquoise',
					'color' => '#AFE2E6',
				),
				array(
					'name'  => __( 'Fioletowy', 'twentynineteen' ),
					'slug'  => 'pale-violet',
					'color' => '#EDDEFC',
				),
				array(
					'name'  => __( 'Żółty', 'twentynineteen' ),
					'slug'  => 'pale-yellow',
					'color' => '#FDF7DF',
				),
				array(
					'name'  => __( 'Czarny', 'twentynineteen' ),
					'slug'  => 'black',
					'color' => '#000',
				),
			)
		);

		// Add support for responsive embedded content.
		add_theme_support( 'responsive-embeds' );
	}
endif;
add_action( 'after_setup_theme', 'frontendhouse_setup' );


/**
 * Fix skip link focus in IE11.
 *
 * This does not enqueue the script because it is tiny and because it is only for IE11,
 * thus it does not warrant having an entire dedicated blocking script being loaded.
 *
 * @link https://git.io/vWdr2
 */
function twentynineteen_skip_link_focus_fix() {
	// The following is minified via `terser --compress --mangle -- js/skip-link-focus-fix.js`.
	?>
	<script>
	/(trident|msie)/i.test(navigator.userAgent)&&document.getElementById&&window.addEventListener&&window.addEventListener("hashchange",function(){var t,e=location.hash.substring(1);/^[A-z0-9_-]+$/.test(e)&&(t=document.getElementById(e))&&(/^(?:a|select|input|button|textarea)$/i.test(t.tagName)||(t.tabIndex=-1),t.focus())},!1);
	</script>
	<?php
}
add_action( 'wp_print_footer_scripts', 'twentynineteen_skip_link_focus_fix' );

/**
 * Enqueue supplemental block editor styles.
 */
function twentynineteen_editor_customizer_styles() {

	wp_enqueue_style( 'twentynineteen-editor-customizer-styles', get_theme_file_uri( '/style-editor-customizer.css' ), false, '1.1', 'all' );

	if ( 'custom' === get_theme_mod( 'primary_color' ) ) {
		// Include color patterns.
		require_once get_parent_theme_file_path( '/inc/color-patterns.php' );
		wp_add_inline_style( 'twentynineteen-editor-customizer-styles', twentynineteen_custom_colors_css() );
	}
}
add_action( 'enqueue_block_editor_assets', 'twentynineteen_editor_customizer_styles' );




/**
 * Enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Custom template tags for the theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Zmiana nazwa Bloga
 */
require get_template_directory() . '/inc/customizer.php';

add_filter( 'get_the_archive_title', function ($title) {
	if ( is_archive() ):
		$title = get_queried_object()->label;
	endif; 

	return $title;
});


// Blog
function blog_page( $query ) {
	if ( $query->is_main_query() && !is_admin() && is_post_type_archive( 'blog' ) ) {
	    if (!empty($_GET["kategoria"])) {
			$query->set( 'tax_query',
        		array(
					array(
						'taxonomy' => 'category',
						'field' => 'slug',
						'terms' => $_GET['kategoria'],
					)
				));
		}
		if (!empty($_GET["produkt"])) {
			$query->set( 'meta_query',
        		array(
					array(
						'key'     => 'powiazane_urzadzenie',
                        'value'   => $_GET["produkt"],
                        'compare' => 'LIKE',
					)
				));
		}
    }
}
add_filter( 'pre_get_posts', 'blog_page' );
 
// Ukrywanie admin bar dla kliniki
add_action('after_setup_theme', 'remove_admin_bar');
 
function remove_admin_bar() {
	if (!current_user_can('administrator') && !is_admin()) {
	show_admin_bar(false);
	}
}

/**
 * @packageStop_Redirect
 */
/*
Plugin Name: Stop redirect
Plugin URI: 
Description: Stop redirecting anything to wp-login
Author: Tomasz Struczyński
Version: 0.1
Author URI: 
*/

add_action('init', 'remove_default_redirect');
add_filter('auth_redirect_scheme', 'stop_redirect', 9999);

function stop_redirect($scheme)
{
    if ( $user_id = wp_validate_auth_cookie( '',  $scheme) ) {
        return $scheme;
    }

    global $wp_query;
    $wp_query->set_404();
    get_template_part( 404 );
    exit();
}

function remove_default_redirect()
{
    remove_action('template_redirect', 'wp_redirect_admin_locations', 1000);
}

/**
 * Redirect back to homepage and not allow access to 
 * WP admin for Subscribers.
 */
function themeblvd_redirect_admin(){
    if ( ! defined('DOING_AJAX') && ! current_user_can('edit_posts') ) {
        wp_redirect( get_permalink(681) );
        exit;       
    }
}
add_action( 'admin_init', 'themeblvd_redirect_admin' );

add_action('admin_head', 'ukrywanie_boxow');

function ukrywanie_boxow() {
  echo '<style>
  .post-type-clinic #tagsdiv-appliances_clinics, .post-type-clinic #tagsdiv-treatments { display: none; } 
  </style>';
}

// edycja 17.02

function style_script() {
	wp_enqueue_style( 'custom', get_template_directory_uri() . '/style_custom.css', '', filemtime(get_stylesheet_directory() . '/style_custom.css' ));
}
add_action( 'wp_enqueue_scripts', 'style_script' );

// edycja 17.02


