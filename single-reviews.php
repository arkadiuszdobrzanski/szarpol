<?php get_header(); ?>
<article class="single-review">
		<?php while ( have_posts() ) :
			the_post(); ?>

			<section class="post-header">
				<div class="background" style="background-image: url('<?php the_post_thumbnail_url('full'); ?>')"></div>
				<div class="container-narrow">
					<div class="article-meta">
						<span class="category">
							Opinia
						</span>
						<div class="line">
							<hr>
						</div>
						<span class="date">
							<?php the_date('d.m.Y'); ?>
						</span>
					</div>
					<?php the_title('<h1>', '</h1>'); ?>
				</div>
			</section>

			<section class="post-content">
				<div class="container-narrow">
					<?php the_content(); ?>
				</div>
			</section>

		<?php endwhile; ?>
</article>
<?php get_footer();