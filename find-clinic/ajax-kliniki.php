<?php
global $wpdb;

$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
require_once( $parse_uri[0] . 'wp-load.php' );

if (!empty($_POST['wojewodztwo'])):

	if (!empty($_POST['zabiegi'])): 
		$zabiegi = array(
			'key'       => 'sharpol_inne_zabiegi_spoza',
			'value'     => $_POST['zabiegi'],
			'compare'   => 'LIKE',
		);
	endif;

	if (!empty($_POST['urzadzenia'])):
		
		if (strpos($_POST['urzadzenia'], 's') !== false) {
			$urzadzenia = array(
				'key'       => 'sharpol_urzadzenia',
				'value'     => ltrim($_POST['urzadzenia'], 's'),
				'compare'   => 'LIKE',
			);
		} else {
			$urzadzenia = array(
				'key'       => 'sharpol_inne_urzadzenia_spoza',
				'value'     => ltrim($_POST['urzadzenia'], 'o'),
				'compare'   => 'LIKE',
			);
		}

	endif;

	$posts = get_posts(array(
		'posts_per_page'	=> -1,
		'post_type'	=> 'clinic',
		'orderby' => 'title',
		'order' => 'ASC',
		'tax_query' => array(
			array(
				'taxonomy' => 'wojewodztwo',
				'field' => 'id',
				'terms' => $_POST['wojewodztwo'],
				'include_children' => false
			)
		), 
		'meta_query'    => array(
			'relation' => 'AND',
			$zabiegi,
			$urzadzenia
		)
	));

	if (!empty($posts)):
		
	foreach( $posts as $post ): 
		
		setup_postdata( $post ); ?>

		<li data-url="<?php echo get_field('adres')['google_map']?>">
			<?php the_title('<h3>', '</h3>'); ?>

			<!-- Dane kontaktowe -->
			<?php if (!empty(get_field('adres')['ulica']) && get_field('adres')['miejscowosc']): ?>
				<p class="adres"><?php echo get_field('adres')['ulica'].', '.get_field('adres')['kod_pocztowy'].' '.get_field('adres')['miejscowosc']; ?></p>
			<?php endif; ?>

			<?php

			if (!empty(get_field('kontakt')['telefon'])):
				echo '<p class="telefon"><a href="tel:'.get_field('kontakt')['telefon'].'">'.get_field('kontakt')['telefon'].'</a></p>';
			endif;

			if (!empty(get_field('kontakt')['e-mail'])):
				echo '<p class="mail"><a href="mailto:'.get_field('kontakt')['e-mail'].'">'.get_field('kontakt')['e-mail'].'</a></p>';
			endif;

			if (!empty(get_field('kontakt')['strona_www'])):
				echo '<p class="link"><a href="'.get_field('kontakt')['strona_www'].'" target="_blank">'.get_field('kontakt')['strona_www'].'</a></p>';
			endif;

			if( have_rows('kontakt_kontakt') ):
			   while ( have_rows('kontakt_kontakt') ) : the_row();
				   if( get_row_layout() == 'telefon' ):
		   
						if (!empty(get_sub_field('telefon'))):
							echo '<p class="telefon"><a href="tel:'.get_sub_field('telefon').'">'.get_sub_field('telefon').'</a></p>';
						endif;

					elseif( get_row_layout() == 'telefon' ):
		   
						if (!empty(get_sub_field('fax'))):
							echo '<p class="telefon">'.get_sub_field('fax').'</p>';
						endif;
		   
				   elseif( get_row_layout() == 'e-mail' ): 

						if (!empty(get_sub_field('e-mail'))):
							echo '<p class="mail"><a href="mailto:'.get_sub_field('e-mail').'">'.get_sub_field('e-mail').'</a></p>';
						endif;

					elseif( get_row_layout() == 'strona_www' ): 

						if (!empty(get_sub_field('strona_www'))):
							echo '<p class="link"><a href="'.get_sub_field('strona_www').'" target="_blank">'.get_sub_field('strona_www').'</a></p>';
						endif;
		   
				   endif;
			   endwhile;
			endif; ?> 

			<?php if (!empty(get_field('telefon', 'user_'.$clinic->ID))): ?>
				<p class="telefon"><?php the_field('telefon', 'user_'.$clinic->ID) ?></p>
			<?php endif; ?>
			<?php if (!empty(get_field('strona_www', 'user_'.$clinic->ID))): ?>
				<p class="link"><a href="<?php the_field('strona_www', 'user_'.$clinic->ID) ?>" target="_blank"><?php the_field('strona_www', 'user_'.$clinic->ID) ?></a></p>
			<?php endif; 
			if (!empty(get_field('adres')['google_map'])): ?>
				<small>Kliknij, aby zobaczyć na mapie</small>
			<?php endif; ?>

		</li>
	
	<?php endforeach; ?>
	
	
	<?php wp_reset_postdata(); ?>
	
	<?php else: ?>
		<p>Brak wyników spełniających dane kryteria</p></li>
		<?php endif; ?>


<?php else: ?>

	<p>Aby wyszukać klinikę wybierz województwo oraz następnie interesujacy zabieg i/lub urządzenie.</p></li>

<?php endif; ?>