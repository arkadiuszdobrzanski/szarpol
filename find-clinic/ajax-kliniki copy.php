<?php
global $wpdb;

$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
require_once( $parse_uri[0] . 'wp-load.php' );

$posts = get_posts(array(
	'posts_per_page'	=> -1,
	'post_type'	=> 'clinic',
	'meta_key'		=> 'adres_wojewodztwo',
	'meta_value'	=> $_POST['wojewodztwo'],
	'orderby' => 'title',
	'order' => 'ASC',
	'tax_query' => array(
		array(
			'taxonomy' => 'wojewodztwo',
			'field' => 'id',
			'terms' => $_POST['wojewodztwo'],
			'include_children' => false
		)
	)
));

if( $posts ): ?>
		
	<?php foreach( $posts as $post ): 
		
		setup_postdata( $post ); ?>

		<li>
			<?php the_title('<h3>', '</h3>'); ?>


			<!-- Zabiegi -->
			<?php //get_field('sharpol')['zabiegi'];
			$zabiegi_sharpol = array();
			foreach (get_field('sharpol')['zabiegi'] as $key => $zabieg) {
				$zabiegi_sharpol[$key]['name'] = $zabieg->post_title;
				$zabiegi_sharpol[$key]['url'] = $zabieg->guid;
			};

			$inne_zabiegi = array();
			foreach (get_field('inne')['zabiegi_spoza'] as $key => $zabieg_inny) {
				$inne_zabiegi[$key]['name'] = $zabieg_inny->name;
				$inne_zabiegi[$key]['url'] = '';
			};

			$zabiegi = array_merge($inne_zabiegi, $zabiegi_sharpol);

			$columns_zabiegi = array_column($zabiegi, 'name');
			array_multisort($columns_zabiegi, SORT_ASC, $zabiegi);

			if ($zabiegi) { 
				echo '<ul class="zabiegi">';
				foreach ($zabiegi as $zabieg) {
					if (!empty($zabieg['url'])):
						echo '<li><a href="'.$zabieg['url'].'" target="_blank">'.$zabieg['name'].'</a></li>';
					else: 
						echo '<li>'.$zabieg['name'].'</li>';
					endif;
				}
				echo '</ul>';
			}
			?>


			<!-- Urządzenia -->
			<?php 
			$urzadzenia_sharpol = array();
			foreach (get_field('sharpol')['urzadzenia'] as $key => $urzadzenie) {
				$urzadzenia_sharpol[$key]['name'] = $urzadzenie->post_title;
				$urzadzenia_sharpol[$key]['url'] = $urzadzenie->guid;
			};

			$inne_urzadzenia = array();
			foreach (get_field('inne')['urzadzenia_spoza'] as $key => $urzadzenie_inny) {
				$inne_urzadzenia[$key]['name'] = $urzadzenie_inny->name;
				$inne_urzadzenia[$key]['url'] = '';
			};

			$urzadzenia = array_merge($inne_urzadzenia, $urzadzenia_sharpol);

			$columns_urzadzenia = array_column($urzadzenia, 'name');
			array_multisort($columns_urzadzenia, SORT_ASC, $urzadzenia);

			if ($urzadzenia) { 
				echo '<ul class="urzadzenia">';
				foreach ($urzadzenia as $urzadzenie) {
					if (!empty($urzadzenie['url'])):
						echo '<li><a href="'.$urzadzenie['url'].'" target="_blank">'.$urzadzenie['name'].'</a></li>';
					else: 
						echo '<li>'.$urzadzenie['name'].'</li>';
					endif;
				}
				echo '</ul>';
			}
			?>


			<!-- Dane kontaktowe -->
			<?php if (!empty(get_field('adres')['ulica']) && get_field('adres')['miejscowosc']): ?>
				<p class="adres"><?php echo get_field('adres')['ulica'].', '.get_field('adres')['kod_pocztowy'].' '.get_field('adres')['miejscowosc']; ?></p>
			<?php endif; ?>


			<?php
			if( have_rows('kontakt') ):
			   while ( have_rows('kontakt') ) : the_row();
				   if( get_row_layout() == 'telefon' ):
		   
						if (!empty(get_sub_field('telefon'))):
							echo '<p class="telefon"><a href="tel:'.get_sub_field('telefon').'">'.get_sub_field('telefon').'</a></p>';
						endif;

					elseif( get_row_layout() == 'telefon' ):
		   
						if (!empty(get_sub_field('fax'))):
							echo '<p class="telefon">'.get_sub_field('fax').'</p>';
						endif;
		   
				   elseif( get_row_layout() == 'e-mail' ): 

						if (!empty(get_sub_field('e-mail'))):
							echo '<p class="mail"><a href="mailto:'.get_sub_field('e-mail').'">'.get_sub_field('e-mail').'</a></p>';
						endif;

					elseif( get_row_layout() == 'strona_www' ): 

						if (!empty(get_sub_field('strona_www'))):
							echo '<p class="link"><a href="'.get_sub_field('strona_www').'" target="_blank">'.get_sub_field('strona_www').'</a></p>';
						endif;
		   
				   endif;
			   endwhile;
			endif; ?> 

			<?php if (!empty(get_field('telefon', 'user_'.$clinic->ID))): ?>
				<p class="telefon"><?php the_field('telefon', 'user_'.$clinic->ID) ?></p>
			<?php endif; ?>
			<?php if (!empty(get_field('strona_www', 'user_'.$clinic->ID))): ?>
				<p class="link"><a href="<?php the_field('strona_www', 'user_'.$clinic->ID) ?>" target="_blank"><?php the_field('strona_www', 'user_'.$clinic->ID) ?></a></p>
			<?php endif; ?>


		</li>
	
	<?php endforeach; ?>
	
	<?php wp_reset_postdata(); ?>

<?php else: ?>
	<li>Wybierz województwo, aby kontynuować 2</li>
<?php endif; ?>