<?php
global $wpdb;

$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
require_once( $parse_uri[0] . 'wp-load.php' );

$posts = get_posts(array(
	'posts_per_page'	=> -1,
	'post_type'	=> 'clinic',
	'meta_key'		=> 'adres_wojewodztwo',
	'meta_value'	=> $_POST['wojewodztwo'],
	'orderby' => 'title',
	'order' => 'ASC',
	'tax_query' => array(
		array(
			'taxonomy' => 'wojewodztwo',
			'field' => 'id',
			'terms' => $_POST['wojewodztwo'],
			'include_children' => false
		)
	)
));

if( $posts ): ?>
		
	<?php foreach( $posts as $post ): 
	setup_postdata( $post );

	$zabiegi_sharpol = array();
	foreach (get_field('sharpol')['zabiegi'] as $zabieg) {
		$zabiegi_sharpol[] = array('name' => $zabieg->post_title, 'id' => $zabieg->ID);
	};

	
	endforeach; ?>
	
	<?php wp_reset_postdata(); ?>

<?php else: ?>
	<li>Wybierz województwo, aby kontynuować 3</li>
<?php endif; ?>