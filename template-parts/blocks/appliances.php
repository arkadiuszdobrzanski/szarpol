<?php
/**
 * Block Name: Zastosowania urządzenia
 *
 */

 // create id attribute for specific styling
$id = 'urzadzenia-' . $block['id'];

// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';
?>

<?php if (!empty(get_the_ID())): ?>
    <?php

    if (get_post_type() == 'blog' || get_post_type() == 'reviews'  || get_post_type() == 'aktualnosci'):

        if (!empty(get_field('powiazane_urzadzenie', get_the_ID()))): ?>

            </div>
            <section id="<?php echo $id; ?>" class="urzadzenia urzadzenia-wide gutenberg-block <?php echo $align_class; ?> <?php echo 'kolor-'.get_field('kolor'); ?>">

                <div class="container-narrow">
                    <h4>Produkty</h4>
                </div>

                <div class="wrapper">
                    <div class="carousel-wrapper">
                        <?php
						
                        $loop = new WP_Query( array( 
                            'post_type' => 'appliance',
                            'post__in' => get_field('powiazane_urzadzenie', get_the_ID()),
                            'ignore_sticky_posts' => 1,
                            'posts_per_page' => -1,
                            'post_status' => array('publish'),
                        ) );
                        if ( $loop->have_posts() ) :
                            while ( $loop->have_posts() ) : $loop->the_post(); ?>
                                <a href="<?php the_permalink(); ?>" class="product">
                                    <figure>
                                        <?php the_post_thumbnail('thumbnail'); ?>
                                        <h3><?php the_title();  ?></h3>
                                        <span><?php the_field('sub_name', get_the_ID()); ?></span>
                                    </figure>
                                </a>
                            <?php endwhile;
                        endif;
                        wp_reset_postdata(); ?>
                    </div>
                </div>
            </section>
            <div class="container-narrow">

        <?php endif;
    else: ?>

    <section id="<?php echo $id; ?>" class="urzadzenia gutenberg-block <?php echo $align_class; ?> <?php echo 'kolor-'.get_field('kolor'); ?>">
    <h4>Produkty</h4>
        <div class="wrapper">
            <div class="carousel-wrapper">
                <?php
                $loop = new WP_Query( array( 
                    'post_type' => 'appliance',
                    'ignore_sticky_posts' => 1,
                    'posts_per_page' => -1,
                    'post_status' => array('publish'),
                    'meta_query' => array(
                        array(
                           'key'     => 'zabiegi',
                           'value'   => '"'.get_the_ID().'"',
                           'compare' => 'LIKE',
                        ),
                      ),

                ) );
                if ( $loop->have_posts() ) :
                    while ( $loop->have_posts() ) : $loop->the_post(); ?>
                        <a href="<?php the_permalink(); ?>" class="product">
                            <figure>
                                <?php the_post_thumbnail('thumbnail'); ?>
                                <h3><?php the_title();  ?></h3>
                                <span><?php the_field('sub_name', get_the_ID()); ?></span>
                            </figure>
                        </a>
                    <?php endwhile;
                endif;
                wp_reset_postdata(); ?>
            </div>
        </div>
        </section>
    <?php endif; ?>

<?php else: ?>
<section id="<?php echo $id; ?>" class="urzadzenia gutenberg-block <?php echo $align_class; ?> <?php echo 'kolor-'.get_field('kolor'); ?>">
    <div style="width: 100%; background: #F2F3F5; padding: 50px 30px;">
        <h4 style="margin-bottom: 0px">Urządzenia</h4>
        <p>Wyświetla urządzenia powiązane z zabiegiem, aby to zrobić na urądzeniu należy wybrać zabiegi.</p>
    </div>
</section>
<?php endif; ?>
