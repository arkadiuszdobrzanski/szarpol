<?php
/**
 * Block Name: Galeria produktu
 *
 */

 // create id attribute for specific styling
$id = 'galeria-produktu-' . $block['id'];

// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';

// Getting images
$images = get_field('zdjecia_produktu');

?>
<section id="<?php echo $id; ?>" class="galeria-produktu gutenberg-block <?php echo $align_class; ?> <?php echo 'kolor-'.get_field('kolor'); ?>">
    <h4>Galeria</h4>
    <div class="carousel-wrapper">
        <?php if( $images ): ?>
            <?php foreach( $images as $image ): ?>
                <figure>
                    <a href="<?php echo $image['url']; ?>" data-fancybox="gallery"  data-options='{"caption" : "<?php echo $image['description']; ?>"}'>
                            <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
                    </a>
                    <?php if (!empty($image['description'])): ?>
                        <p><?php echo $image['description']; ?></p>
                    <?php endif; ?>
                </figure>
            <?php endforeach; ?>
        <?php else: ?>
            <h5 style="text-align: center; color: #0073AA">Pusto tu! Wrzuć zdjęcia produktu.</h5> 
        <?php endif; ?>
    </div>
</section>
<?php if (empty(get_the_ID())): ?>
<style type="text/css">
#<?php echo $id; ?> {
    min-height: 350px;
    background-color: #F2F3F5;
    padding: 30px;
}
#<?php echo $id; ?> figure {
    display: inline-block;
    width: 200px;
}
</style>
<?php endif; ?>