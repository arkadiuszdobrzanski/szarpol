<?php
/**
 * Block Name: Blok artykuły
 *
 */

 // create id attribute for specific styling
 $id = 'articles-block-' . $block['id'];

 // create align class ("alignwide") from block setting ("wide")
 $align_class = $block['align'] ? 'align' . $block['align'] : '';
 ?>
 
 <section id="<?php echo $id; ?>" class="articles-block gutenberg-block <?php echo $align_class; ?> <?php echo 'kolor-'.get_field('kolor'); ?>">
 <?php if (!empty(get_the_ID())): ?>
     <h4>Blog</h4>
         <div class="wrapper">
             <div class="carousel-wrapper">
                <?php
                $loop = new WP_Query( array( 
                    'post_type' => 'blog',
                    'ignore_sticky_posts' => 1,
                    'posts_per_page' => -1,
                    'post_status' => array('publish'),
                    'meta_query' => array(
                        array(
                           'key'     => 'powiazane_urzadzenie',
                           'value'   => '"'.get_the_ID().'"',
                           'compare' => 'LIKE',
                        ),
                      ),

                ) );
                if ( $loop->have_posts() ) :
                    while ( $loop->have_posts() ) : $loop->the_post(); ?>
                        <a href="<?php the_permalink(); ?>" class="review">
                            <figure>
                                <div class="wrapper">
                                    <div class="image">
                                        <?php the_post_thumbnail('large'); ?>
                                    </div>
                                    <?php if (get_field('format_wyswietlania', get_the_category()[0]) == 'pasek_na_poscie'): ?>
									<hr style="border-color: <?php echo get_field('kolor_paska', get_the_category()[0]); ?>">
                                    <?php else: ?>
                                        <hr>
                                    <?php endif; ?>
                                    <div class="title">
                                        <h3><?php the_title(); ?></h3>
                                        <span class="read-more">Czytaj dalej </span>
                                    </div>
                                </div>
                            </figure>
                        </a>
                    <?php endwhile;
                endif;
                wp_reset_postdata(); ?>

             </div>
         </div>
 <?php else: ?>
 <div style="width: 100%; background: #F2F3F5; padding: 50px 30px;">
     <h4 style="margin-bottom: 0px">Blog</h4>
     <p>Zainteresuj powiązanymi artykułami na blogu.</p>
 </div>
 <?php endif; ?>
 </section>
 