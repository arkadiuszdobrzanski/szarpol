<?php
/**
 * Block Name: Blok opinii
 *
 */

 // create id attribute for specific styling
 $id = 'reviews-block-' . $block['id'];

 // create align class ("alignwide") from block setting ("wide")
 $align_class = $block['align'] ? 'align' . $block['align'] : '';
 ?>
 
 <section id="<?php echo $id; ?>" class="reviews-block gutenberg-block <?php echo $align_class; ?> <?php echo 'kolor-'.get_field('kolor'); ?>">
 <?php if (!empty(get_the_ID())): ?>
     <h4>Opinie</h4>
         <div class="wrapper">
             <div class="carousel-wrapper">
                <?php
                $loop = new WP_Query( array( 
                    'post_type' => 'reviews',
                    'ignore_sticky_posts' => 1,
                    'posts_per_page' => -1,
                    'post_status' => array('publish'),
                    'meta_query' => array(
                        array(
                           'key'     => 'powiazane_urzadzenie',
                           'value'   => '"'.get_the_ID().'"',
                           'compare' => 'LIKE',
                        ),
                      ),

                ) );
                if ( $loop->have_posts() ) :
                    while ( $loop->have_posts() ) : $loop->the_post(); ?>
                        <a href="<?php the_permalink(); ?>" class="review">
                            <figure>
                                <?php the_post_thumbnail('large'); ?>
                                <h3><?php the_title(); ?></h3>
                            </figure>
                        </a>
                    <?php endwhile;
                endif;
                wp_reset_postdata(); ?>

             </div>
         </div>
 <?php else: ?>
 <div style="width: 100%; background: #F2F3F5; padding: 50px 30px;">
     <h4 style="margin-bottom: 0px">Opinie</h4>
     <p>Wyświetla wszystkie opinie powiązane z urządzeniem.</p>
 </div>
 <?php endif; ?>
 </section>
 