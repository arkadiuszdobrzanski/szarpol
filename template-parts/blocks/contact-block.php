<?php
/**
 * Block Name: Blok kontaktu
 *
 */

// create id attribute for specific styling
$id = 'blok-kontaktu-' . $block['id'];

// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';

?>
<section id="<?php echo $id; ?>" class="ask__content blok-kontaktu <?php echo $align_class; ?>">
    <div class="ask__container">
        <div class="row">
            <div class="ask__left col-lg-6 col-md-4">
                <p><?php the_field('tekst'); ?></p>

            </div>
            <div class="ask__right col-lg-6 col-md-8" style="position: relative; height: 620px">
                <div role="form" class="wpcf7" id="wpcf7-f4138-p6450-o1" style="position: absolute;" lang="pl-PL" dir="ltr">
                    <div class="screen-reader-response"><p role="status" aria-live="polite"  style="position: static;"aria-atomic="true"></p> <ul></ul></div>
                    <form action="/produkt/urzadzenie-do-hydradermabrazji-jetpeel-pro/#wpcf7-f4138-p6450-o1"  style="position: static;" method="post" class="wpcf7-form init" novalidate="novalidate" data-status="init">
                        <div style="display: none;">
                            <input type="hidden" name="_wpcf7" value="4138">
                            <input type="hidden" name="_wpcf7_version" value="5.3">
                            <input type="hidden" name="_wpcf7_locale" value="pl_PL">
                            <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f4138-p6450-o1">
                            <input type="hidden" name="_wpcf7_container_post" value="6450">
                            <input type="hidden" name="_wpcf7_posted_data_hash" value="">
                            <input type="hidden" name="_wpcf7_recaptcha_response" value="">
                        </div>
                        <div class="ask__form__data"><span class="wpcf7-form-control-wrap your-data"><input type="text" name="your-data" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false"></span></div>
                        <div id="ask__form" class="flex_col">
                            <h2 class="ask__form__title">Formularz kontaktowy</h2>
                            <div class="ask__form__flex flex_row">
                                <div class="ask__form__contact flex_col">
                                    <div class="ask__form__name flex_row">
                                        <h3>Imię i&nbsp;nazwisko</h3>
                                        <p><span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false"></span>
                                        </p></div>
                                    <div class="ask__form__line"></div>
                                    <div class="ask__form__mail flex_row">
                                        <h3>E-mail</h3>
                                        <p><span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false"></span>
                                        </p></div>
                                    <div class="ask__form__line"></div>
                                    <div class="ask__form__phone flex_row">
                                        <h3>Numer telefonu</h3>
                                        <p><span class="wpcf7-form-control-wrap your-phone"><input type="tel" name="your-phone" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel" aria-required="true" aria-invalid="false"></span>
                                        </p></div>
                                    <div class="ask__form__line"></div>
                                    <div class="ask__form__province flex_row">
                                        <h3>Województwo</h3>
                                        <p><span class="wpcf7-form-control-wrap your-province"><select name="your-province" class="wpcf7-form-control wpcf7-select" aria-invalid="false"><option value="">—</option><option value="dolnośląskie">dolnośląskie</option><option value="kujawsko-pomorskie">kujawsko-pomorskie</option><option value="lubelskie">lubelskie</option><option value="lubuskie">lubuskie</option><option value="łódzkie">łódzkie</option><option value="małopolskie">małopolskie</option><option value="mazowieckie">mazowieckie</option><option value="opolskie">opolskie</option><option value="podkarpackie">podkarpackie</option><option value="podlaskie">podlaskie</option><option value="pomorskie">pomorskie</option><option value="śląskie">śląskie</option><option value="świętokrzyskie">świętokrzyskie</option><option value="warmińsko-mazurskie">warmińsko-mazurskie</option><option value="wielkopolskie">wielkopolskie</option><option value="zachodniopomorskie">zachodniopomorskie</option></select></span>
                                        </p></div>
                                    <div class="ask__form__line"></div>
                                    <p></p></div>
                                <div class="ask__form__text flex_row">
                                    <h3>Treść wiadomości</h3>
                                    <p><span class="wpcf7-form-control-wrap your-message"><textarea name="your-message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></textarea></span>
                                    </p></div>
                                <p></p></div>
                            <div class="ask__form__acceptance flex_row">
        <span class="wpcf7-form-control-wrap your-acceptance"><span class="wpcf7-form-control wpcf7-acceptance"><span class="wpcf7-list-item"><label><input type="checkbox" name="your-acceptance" value="1" aria-invalid="false"><span class="wpcf7-list-item-label"><p>
            Wyrażam zgodę na&nbsp;przetwarzanie danych osobowych zgodnie z&nbsp;ustawą o&nbsp;ochronie danych osobowych, w&nbsp;związku z&nbsp;wysłaniem zapytania przez&nbsp;formularz kontaktowy. Podanie danych jest dobrowolne, ale&nbsp;niezbędne do&nbsp;przetworzenia zapytania. Zostałem poinformowany, że&nbsp;przysługuje mi prawo dostępu do&nbsp;sowich danych, możliwości ich poprawiania, żądania zaprzestania ich przetworzenia. Administratorem danych osobowych jest SHAR-POL Sp. z&nbsp;o. o., 44-102 Gliwice, ul.&nbsp;Św. Małgorzaty 6/1.
        </p>
<p>
    </p></span></label></span></span></span></div>
                            <div class="ask__form__submit">
                                <input type="submit" value="Wyślij wiadomość" class="wpcf7-form-control wpcf7-submit" disabled=""><span class="ajax-loader"></span>
                            </div>
                        </div>
                        <div class="wpcf7-response-output" aria-hidden="true"></div></form></div>
                <div class="ask__check flex_row">
                    <p>Sprawdź numer do lokalnego przedstawiciela</p>
                    <img src="<?php bloginfo('template_url'); ?>/img/arrow.png" alt="arrow">
                </div>
            </div>
        </div>
    </div>
</section>
<style type="text/css">
    #<?php echo $id; ?> {
        min-height: 350px;
        background-image: url('<?php the_field('grafika') ?>');
        background-size: cover;
        background-position: center;
        padding: 4%;
    }
</style>
