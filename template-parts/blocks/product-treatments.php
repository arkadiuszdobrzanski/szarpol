<?php
/**
 * Block Name: Zastosowania urządzenia
 *
 */

 // create id attribute for specific styling
$id = 'zastosowania-urzadzenia-' . $block['id'];

// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';
?>

<section id="<?php echo $id; ?>" class="zastosowania-urzadzenia gutenberg-block <?php echo $align_class; ?> <?php echo 'kolor-'.get_field('kolor'); ?>">
<?php if (!empty(get_the_ID())): ?>
    <h4>Zastosowania</h4>
        <div class="wrapper">
            <div class="carousel-wrapper">
                <?php//var_dump(get_field('zabiegi', get_the_ID())); ?>
                <?php foreach (get_field('zabiegi', get_the_ID()) as $zabieg): ?>
                    <a href="<?php the_permalink($zabieg->ID); ?>" class="treatment">
                        <figure>
                            <?php echo get_the_post_thumbnail($zabieg->ID); ?>
                            <h3><?php echo $zabieg->post_title; ?></h3>
                        </figure>
                    </a>
                <?php endforeach; ?>
            </div>
        </div>
<?php else: ?>
<div style="width: 100%; background: #F2F3F5; padding: 50px 30px;">
    <h4 style="margin-bottom: 0px">Zastosowania</h4>
    <p>Wyświetla w formie listy z miniaturkami zabiegi przyporządkowane do urządzenia.</p>
</div>
<?php endif; ?>
</section>
