<h2 class="products-title">Blog</h2>
<div class="row-alt">
<?php  foreach($data as $blog) { ?>
    <div class="col-3">
        <a href="<?php echo get_the_permalink($blog); ?>">
            <div class="card">
                <div class="card-img-top">
                    <img src="<?php echo get_the_post_thumbnail_url($blog); ?>" alt="">
                </div>
                <div class="card-body">
                    <h2 class="card-title"><?php echo get_the_title($blog); ?></h2>
                    <p class="card-text"><?php echo get_field('sub_name', $blog); ?></p>
                </div>
            </div>
        </a>
    </div>
<?php } ?>
</div>
