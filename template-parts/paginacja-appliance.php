<h2 class="products-title">Produkty</h2>
<div class="row">
<?php  foreach($data as $product) { ?>
    <div class="col-3">
        <a href="<?php echo get_the_permalink($product); ?>">
            <div class="card">
                <div class="card-img-top">
                    <img src="<?php echo get_the_post_thumbnail_url($product); ?>" alt="">
                </div>
                <div class="card-body">
                    <h2 class="card-title"><?php echo get_the_title($product); ?></h2>
                    <p class="card-text"><?php echo get_field('sub_name', $product); ?></p>
                </div>
            </div>
        </a>
    </div>
<?php } ?>
</div>
