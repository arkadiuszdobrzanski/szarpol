
<?php  foreach($data as $new) { ?>
					<?php $category = get_the_category($new);?>
						<article class="<?php echo get_the_category($new)[0]->slug; ?>">

							<?php echo get_field('format_wyswietlania', get_the_category($new)[0]); ?>

							<div class="article-meta">
								<span class="category">
								<?php
									foreach(get_the_category($new) as $cat):
										if (get_field('format_wyswietlania', $cat) == 'nazwa_nad_postem'):
											echo $cat->name.'&nbsp;&nbsp;';
										endif;
									endforeach; ?>
								</span>
								<div class="line">
									<hr>
								</div>
								<?php echo (!empty(get_the_date($new)) ? '<span class="date">'.get_the_date('d.m.Y').'</span>' : '') ?>
							</div>
							<a href="<?php echo get_the_permalink($new); ?>">
								<?php echo get_the_post_thumbnail($new); ?>
								<?php if (get_field('format_wyswietlania', get_the_category($new)[0]) == 'pasek_na_poscie'): ?>
									<hr style="border-color: <?php echo get_field('kolor_paska', get_the_category($new)[0]); ?>">
								<?php else: ?>
									<hr>
								<?php endif; ?>
								<div class="title">
									<h2><?php echo get_the_title($new); ?></h2>
								</div>
								<div class="read-more">
									Czytaj dalej
								</div>
							</a>
						</article>
		
				<?php } ?>