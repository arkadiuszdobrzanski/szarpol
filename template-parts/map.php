<?php foreach ($data as $person) { ?>
  <figure class="trader col-xxl-12 col-xl-12 col-lg-6 col-md-12">
  <h3><?php echo wp_get_post_terms($person->ID, 'medicine')[0]->name; ?></h3>
  <strong><?php echo $person->post_title ?></strong>
  <?php echo $person->post_content ?>
</figure>
<?php } ?>

