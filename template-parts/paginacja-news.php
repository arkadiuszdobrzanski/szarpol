<h2 class="products-title">Aktualności</h2>
<div class="row-alt">
<?php  foreach($data as $news) { ?>
    <div class="col-3">
        <a href="<?php echo get_the_permalink($news); ?>">
            <div class="card">
                <div class="card-img-top">
                    <img src="<?php echo get_the_post_thumbnail_url($news); ?>" alt="">
                </div>
                <div class="card-body">
                    <h2 class="card-title"><?php echo get_the_title($news); ?></h2>
                    <p class="card-text"><?php echo get_field('sub_name', $news); ?></p>
                </div>
            </div>
        </a>
    </div>
<?php } ?>
</div>
