<?php

/**
 * Enqueue scripts and styles.
 */
function frontendhouse_scripts() {
	wp_enqueue_style( 'style', get_stylesheet_uri(), array(), wp_get_theme()->get( 'Version' ) );
	wp_enqueue_style( 'google-font', 'https://fonts.googleapis.com/css?family=Lato:300,400,700,900&amp;subset=latin-ext');
	wp_enqueue_style( 'slick', get_template_directory_uri() . '/css/slick.css', '', filemtime(get_stylesheet_directory() . '/css/slick.css' ));
	wp_enqueue_style( 'scrollbar', get_template_directory_uri() . '/css/jquery.mCustomScrollbar.css', '', filemtime(get_stylesheet_directory() . '/css/jquery.mCustomScrollbar.css' ));
	wp_enqueue_style( 'fancybox', get_template_directory_uri() . '/css/jquery.fancybox.css', '', filemtime(get_stylesheet_directory() . '/css/jquery.fancybox.css' ));
	wp_enqueue_style( 'cssmap-poland', get_template_directory_uri() . '/css/cssmap-poland.css', '', filemtime(get_stylesheet_directory() . '/css/cssmap-poland.css' ));
	wp_enqueue_style( 'toolipster', get_template_directory_uri() . '/css/tooltipster.main.css', '', filemtime(get_stylesheet_directory() . '/css/tooltipster.main.css' ));
	wp_enqueue_style( 'main-style', get_template_directory_uri() . '/css/main.css', '', filemtime(get_stylesheet_directory() . '/css/main.css' ));


	wp_deregister_script('jquery-script');
	wp_deregister_script('jquery');
	wp_register_script('jquery', '//code.jquery.com/jquery-3.2.1.min.js', array(), null);
	wp_enqueue_script('jquery');

	@wp_enqueue_script( 'mixitup', get_theme_file_uri( '/js/mixitup.min.js' ), array(jquery), '3.3.1', true );
	@wp_enqueue_script( 'slick', get_theme_file_uri( '/js/slick.min.js' ), array(jquery), '1.9.0', true );
	@wp_enqueue_script( 'scrollbar', get_theme_file_uri( '/js/jquery.mCustomScrollbar.concat.min.js' ), array(jquery), '3.1.5', true );
	wp_enqueue_script( 'particles-js', get_theme_file_uri( '/js/particles.min.js' ), array(), '2.0.0', true );
	@wp_enqueue_script( 'particles-app', get_theme_file_uri( '/js/particles-app.js' ), array(jquery), '2.0.0', true );
	@wp_enqueue_script( 'parallaxie', get_theme_file_uri( '/js/parallaxie.js' ), array(jquery), '0.5', true );
	@wp_enqueue_script( 'fancybox', get_theme_file_uri( '/js/jquery.fancybox.min.js' ), array(jquery), '3.5.7', true );
	wp_enqueue_script( 'cssmap', get_template_directory_uri() . '/js/jquery.cssmap.min.js', array( 'jquery' ), filemtime(get_stylesheet_directory() . '/js/jquery.cssmap.min.js' ), true );
	@wp_enqueue_script( 'toolipster', get_theme_file_uri( '/js/tooltipster.bundle.min.js' ), array(jquery), '4.2.6', true );
	wp_enqueue_script( 'manifest', get_theme_file_uri( '/favicon/manifest.json' ), array(), '1', true );
	wp_enqueue_script( 'scripts', get_theme_file_uri( '/js/scripts.js' ), array('jquery'), '1', true );
}
add_action( 'wp_enqueue_scripts', 'frontendhouse_scripts' );