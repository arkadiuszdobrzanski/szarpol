<?php

/*

@package weblider

	========================
		USUWANIE NIEPOTRZEBNYCH RZECZY Z WORDPRESS'A
	========================
*/

/* Usuwanie informacji o wersji z js i css */
function bezzo_remove_wp_version_strings( $src ) {
	global $wp_version;
	parse_str( parse_url($src, PHP_URL_QUERY), $query );
	if ( !empty( $query['ver'] ) && $query['ver'] === $wp_version ) {
		$src = remove_query_arg( 'ver', $src );
	}
	return $src;
}
add_filter( 'script_loader_src', 'bezzo_remove_wp_version_strings' );
add_filter( 'style_loader_src', 'bezzo_remove_wp_version_strings' );

/* Remove 'text/css' from our enqueued stylesheet */
function bezzo_style_remove( $tag ) {
    return preg_replace( '~\s+type=["\'][^"\']++["\']~', '', $tag );
}
add_filter( 'style_loader_tag', 'bezzo_style_remove' );


function bezzo_disable_emojicons_tinymce( $plugins ) {
    if ( is_array( $plugins ) ) {
        return array_diff( $plugins, array( 'wpemoji' ) );
    } else {
        return array();
    }
}


/* Wyłaczanie emoji */
function taco_disable_wp_emoji() {
    remove_action( 'admin_print_styles', 'print_emoji_styles' );
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
    remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
    remove_action( 'wp_print_styles', 'print_emoji_styles' );
    remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
    remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
    remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
    add_filter( 'tiny_mce_plugins', 'bezzo_disable_emojicons_tinymce' );
}
add_action( 'init', 'taco_disable_wp_emoji' );


/* Czyszczenie nagłówka */
function taco_wp_clear_head() {
	remove_action( 'wp_head', 'feed_links_extra', 3 );
	remove_action( 'wp_head', 'feed_links', 2 );
	remove_action( 'wp_head', 'rsd_link' );
	remove_action( 'wp_head', 'wlwmanifest_link' );
	remove_action( 'wp_head', 'index_rel_link' );
	remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
	remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
    remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0 );
	remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 );
	remove_action( 'wp_head', 'wp_generator' );
    remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
    remove_action( 'wp_head', 'wp_oembed_add_discovery_links', 10 );
    remove_action( 'xmlrpc_rsd_apis', 'rest_output_rsd' );
    remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
    remove_action( 'template_redirect', 'rest_output_link_header', 11 );
    remove_action( 'wp_head', 'wp_oembed_add_host_js' );
    remove_action('welcome_panel', 'wp_welcome_panel');
}
add_action('init', 'taco_wp_clear_head');


// Disable support for comments and trackbacks in post types
function bezzo_disable_comments_post_types_support() {
	$post_types = get_post_types();
	foreach ($post_types as $post_type) {
		if(post_type_supports($post_type, 'comments')) {
			remove_post_type_support($post_type, 'comments');
			remove_post_type_support($post_type, 'trackbacks');
		}
	}
}
add_action('admin_init', 'bezzo_disable_comments_post_types_support');


// Close comments on the front-end
function bezzo_disable_comments_status() {
	return false;
}
add_filter('comments_open', 'bezzo_disable_comments_status', 20, 2);
add_filter('pings_open', 'bezzo_disable_comments_status', 20, 2);


// Hide existing comments
function bezzo_disable_comments_hide_existing_comments($comments) {
	$comments = array();
	return $comments;
}
add_filter('comments_array', 'bezzo_disable_comments_hide_existing_comments', 10, 2);


// Remove comments page in menu
function bezzo_disable_comments_admin_menu() {
	remove_menu_page('edit-comments.php');
}
add_action('admin_menu', 'bezzo_disable_comments_admin_menu');


// Redirect any user trying to access comments page
function bezzo_disable_comments_admin_menu_redirect() {
	global $pagenow;
	if ($pagenow === 'edit-comments.php') {
		wp_redirect(admin_url()); exit;
	}
}
add_action('admin_init', 'bezzo_disable_comments_admin_menu_redirect');


// Remove comments metabox from dashboard
function bezzo_disable_comments_dashboard() {
	remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');
}
add_action('admin_init', 'bezzo_disable_comments_dashboard');


// Remove comments links from admin bar
function bezzo_disable_comments_admin_bar() {
	if (is_admin_bar_showing()) {
		remove_action('admin_bar_menu', 'wp_admin_bar_comments_menu', 60);
	}
}
add_action('init', 'bezzo_disable_comments_admin_bar');


// Disable displaying users in rest API
add_filter( 'rest_endpoints', function( $endpoints ){
    if ( isset( $endpoints['/wp/v2/users'] ) ) {
        unset( $endpoints['/wp/v2/users'] );
    }
    if ( isset( $endpoints['/wp/v2/users/(?P<id>[\d]+)'] ) ) {
        unset( $endpoints['/wp/v2/users/(?P<id>[\d]+)'] );
    }
    return $endpoints;
});


// Wyłączanie xmlrpc
add_filter( 'xmlrpc_enabled', '__return_false' );
add_filter( 'wp_headers', 'bezzo_remove_x_pingback' );
function bezzo_remove_x_pingback( $headers ) {
    unset( $headers['X-Pingback'] );
    return $headers;
}


// Wyłączanie ping
add_filter( 'xmlrpc_methods', 'bezzo_remove_ping' );
function bezzo_remove_ping( $methods ) {
   unset( $methods['pingback.ping'] );
   unset( $methods['pingback.extensions.getPingbacks'] );
   return $methods;
}


add_filter( 'wp_headers', 'bezzo_remove_pingback_headers' );
function bezzo_remove_pingback_headers( $headers ) {
   unset( $headers['X-Pingback'] );
   return $headers;
}


function my_remove_wp_seo_meta_box() {
	remove_meta_box('wpseo_meta', 'clinic', 'normal');
}
add_action('add_meta_boxes', 'my_remove_wp_seo_meta_box', 100);


add_action( 'init', function() {
    remove_post_type_support( 'clinic', 'editor' );
}, 99);