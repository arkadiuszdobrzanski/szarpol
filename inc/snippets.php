<?php

// snippets::posts per page
function snippet_query( $query ){
    if( ! is_admin()
        && $query->is_post_type_archive( 'snippet' )
        && $query->is_main_query() ){
            $query->set( 'posts_per_page', -1 );
    }
}
add_action( 'pre_get_posts', 'snippet_query' );
