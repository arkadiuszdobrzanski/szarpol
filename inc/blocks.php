<?php
// filter for Frontend output.
add_filter( 'lazyblock/karuzela-portfolio/frontend_callback', 'karuzela_portfolio_frontend', 10, 2 );
// filter for Editor output.
add_filter( 'lazyblock/karuzela-portfolio/editor_callback', 'my_block_output', 10, 2 );
if ( ! function_exists( 'my_block_output' ) ) :
    /**
     * Test Render Callback
     *
     * @param string $output - block output.
     * @param array  $attributes - block attributes.
     */
    function my_block_output( $output, $attributes ) {
        ob_start();
        ?>

        <div>
            Control value: <?php echo esc_html( $attributes['test_control'] ); ?>
        </div>

        <?php
        return ob_get_clean();
    }
endif;

if ( ! function_exists( 'karuzela_portfolio_frontend' ) ) :
    function karuzela_portfolio_frontend( $output, $attributes ) {
        ob_start();
        ?>

			<div id="karuzela-portfolio">
				<div class="owl-carousel" id="portfolio-carousel">

				<?php
				$portfolio = new WP_Query( array(
					'post_type' => 'portfolio',
					'ignore_sticky_posts' => 1,
					'orderby' => 'date',
					'order'   => 'DESC',
					'posts_per_page' => 10,
					'post_status' => 'publish'
				));
				if ( $portfolio->have_posts() ):
					$counter = 0;
					while ( $portfolio->have_posts() ) : $portfolio->the_post();

						$item_terms = '';
						$terms = get_the_terms( get_the_ID(), 'zakres' );
						if ( ! empty( $terms ) && ! is_wp_error( $terms ) ):
							foreach ( $terms as $term ):
						        $item_terms .= $term->name.', ';
						    endforeach;
					    endif;

						echo '<a href="'.get_permalink().'" class="item '.get_post_field( 'post_name', get_post() ).'" data-tasks="'.rtrim($item_terms,", ").'" >';


							if ( has_post_thumbnail()):
									the_post_thumbnail();
							endif;
							echo '<div class="company">';
								the_title('<h5>','</h5>');
							echo '</div></a>';

					endwhile;
				endif;  ?>

				</div>
				<div class="description">
					<div class="wrapper">
						<h4></h4>
						<ul class="tasks"></ul>

						<a href="" title="Zobacz realizacę">Zobacz realizację</a>

					</div>
					<ul class="portfolio-pager">
						<li class="previous">poprzedni</li>
						<li class="index">
							<ul></ul>
						</li>
						<li class="next">następny</li>
					</ul>
				</div>
			</div>

        <?php
        return ob_get_clean();
    }
endif;