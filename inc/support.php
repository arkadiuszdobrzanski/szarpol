<?php

// Remove some wp features
add_action( 'after_setup_theme', 'generic_setup' );
function generic_setup() {

    remove_image_size('medium');
    remove_image_size('medium_large');
    remove_image_size('large');

    add_theme_support( 'html5', array( 'search-form' ) );
}

// Remove admin pages
function remove_menus(){
    remove_menu_page( 'upload.php' );
    remove_menu_page( 'edit.php' );
}
add_action( 'admin_menu', 'remove_menus' );

// Reset upload size
@ini_set( 'upload_max_size' , '64M' );
@ini_set( 'post_max_size', '64M');
@ini_set( 'max_execution_time', '300' );

