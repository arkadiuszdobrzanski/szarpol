<?php

/* Rejestrowanie menu */

  function register_menus() {
    register_nav_menu( 'primary',_( 'Menu główne' ) );
    register_nav_menu( 'footer',_( 'Menu w stopce' ) );
    register_nav_menu( 'bottom',_( 'Menu pod stopką' ) );
  }
  add_action( 'init', 'register_menus' );


/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */

  function arphabet_widgets_init() {

    register_sidebar(
      array(
        'name'          => 'Footer',
        'id'            => 'footer',
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
        'before_title'  => '<span style="display: none;">',
        'after_title'   => '</span>',
      ));
      register_sidebar(
      array(
        'name'          => 'Kontakt w stopce',
        'id'            => 'contact',
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
        'before_title'  => '<span style="display: none;">',
        'after_title'   => '</span>',
      ));
      register_sidebar(
        array(
          'name'          => 'Widget nad footerem',
          'id'            => 'above_footer',
          'before_widget' => '<div>',
          'after_widget'  => '</div>',
          'before_title'  => '<span style="display: none;">',
          'after_title'   => '</span>',
        ));
  
  }
  add_action( 'widgets_init', 'arphabet_widgets_init' );


/* Usuwanie standardowych widgetów */
function bezzo_deregister_widgets() {
    unregister_widget('WP_Widget_Pages');		// Lista stron w serwisie.
    unregister_widget('WP_Widget_Calendar');	// Kalendarz z wpisami.
    unregister_widget('WP_Widget_Archives');	// Archiwum wpisów, podzielone na miesiące.
    unregister_widget('WP_Widget_Links');		//
    unregister_widget('WP_Widget_Meta');		// Odnośniki do: logowania, panelu administracji i WordPress.org.
    unregister_widget('WP_Widget_Search');		// Formularz wyszukiwania dla serwisu.
    unregister_widget('WP_Widget_Text');		// Dowolny tekst lub kod HTML.
    unregister_widget('WP_Widget_Categories');	// Lista lub rozwijalne menu z listą kategorii
    unregister_widget('WP_Widget_Recent_Posts');	// Najnowsze wpisy na stronie.
    unregister_widget('WP_Widget_Recent_Comments');	// Najnowsze komentarze umieszczone na stronie.
    unregister_widget('WP_Widget_RSS');		// Wpisy z dowolnego kanału RSS lub Atom.
    unregister_widget('WP_Widget_Tag_Cloud');	// Chmura najczęściej używanych tagów.
    unregister_widget('WP_Nav_Menu_Widget');	// Dodaj własne menu do panelu bocznego.
}



// Custom WP widget

if(!class_exists('CtaWidget')) {

  class CtaWidget extends WP_Widget {

    /**
    * Sets up the widgets name etc
    */
    public function __construct() {
      $widget_ops = array(
        'classname' => 'ask_for',
        'description' => 'Umieść graficzny widget zachęcający do kontaku.',
      );
      parent::__construct( 'ask_for', 'Zapytaj o produkt', $widget_ops );
    }

    /**
    * Outputs the content of the widget
    *
    * @param array $args
    * @param array $instance
    */
    public function widget( $args, $instance ) {
      // outputs the content of the widget
      if ( ! isset( $args['widget_id'] ) ) {
        $args['widget_id'] = $this->id;
      }

      // widget ID with prefix for use in ACF API functions
      $widget_id = 'widget_' . $args['widget_id']; 
      $show_on_ids = get_field('pokaz_na_stronach', $widget_id);

      if( in_array( get_queried_object()->ID, wp_list_pluck($show_on_ids, 'ID') ) ):

        ?> <section id="<?php echo $widget_id; ?>" class="blok-kontaktu alignfull" style="background-image: url('<?php the_field('grafika', $widget_id) ?>'); padding: 4%;">
          <div class="container-narrow">
              <p><?php the_field('tekst', $widget_id); ?></p>
              <a class="button" href="<?php echo esc_url(get_field('link', $widget_id)['url']); ?>" target="<?php echo get_field('link', $widget_id)['target'] ? get_field('link', $widget_id)['target'] : '_self' ?>"><?php echo esc_html(get_field('link', $widget_id)['title']); ?></a>
          </div>
        </section> <?php
        endif;


    }

    /**
     * Outputs the options form on admin
     *
     * @param array $instance The widget options
     */
    public function form( $instance ) {
    	// outputs the options form on admin
    }

    /**
     * Processing widget options on save
     *
     * @param array $new_instance The new options
     * @param array $old_instance The previous options
     *
     * @return array
     */
    public function update( $new_instance, $old_instance ) {
    	// processes widget options to be saved
    }

  }

}

/**
 * Register our CTA Widget
 */
function register_cta_widget()
{
  register_widget( 'CtaWidget' );
}
add_action( 'widgets_init', 'register_cta_widget' );


function my_mario_block_category( $categories, $post ) {
	return array_merge(
		$categories,
		array(
			array(
				'slug' => 'sharpol',
				'title' => __( 'Shar-Pol', 'sharpol' ),
			),
		)
	);
}
add_filter( 'block_categories', 'my_mario_block_category', 10, 2);




/**
 * Register: blok kontaktu
 */
add_action('acf/init', 'my_acf_init');
function my_acf_init() {
	
	// check function exists
	if( function_exists('acf_register_block') ) {
		
		// register a testimonial block
		acf_register_block(
      array(
			'name'				=> 'contact-block',
			'title'				=> __('Blok kontaktu'),
			'description'		=> __('Dodaj efektywny blok kontaktu wraz z grafiką w tle!'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'sharpol',
			'icon'				=> 'phone',
			'keywords'			=> array( 'kontakt' ),
      )
  );
    acf_register_block(
      array(
      'name'				=> 'reviews-block',
      'title'				=> __('Opinie o produkcie'),
      'description'		=> __('Udostępnij informacje o produkcie na jego stronie.'),
      'render_callback'	=> 'my_acf_block_render_callback',
      'category'			=> 'sharpol',
      'icon'				=> 'admin-comments',
      'keywords'			=> array( 'opinie', 'opinia', 'recenzje' ),
      )
  );
  acf_register_block(
    array(
    'name'				=> 'image-carousel',
    'title'				=> __('Galeria zdjęć'),
    'description'		=> __('Pokaż zdjecia produktu'),
    'render_callback'	=> 'my_acf_block_render_callback',
    'category'			=> 'sharpol',
    'icon'				=> 'images-alt',
    'keywords'			=> array( 'zdjęcia', 'zdjecie', 'produkt', 'galeria' ),
    )
);
  acf_register_block(
    array(
    'name'				=> 'articles-block',
    'title'				=> __('Artykuły o produkcie'),
    'description'		=> __('Wyświetla wszystkie artykuły, dotyczące produktu.'),
    'render_callback'	=> 'my_acf_block_render_callback',
    'category'			=> 'sharpol',
    'icon'				=> 'media-document',
    'keywords'			=> array( 'blog', 'wpisy' ),
    )
  );
  acf_register_block(
    array(
    'name'				=> 'product-treatments',
    'title'				=> __('Zastosowania'),
    'description'		=> __('Wyświetla wybrane zabiegi dla urządzenia.'),
    'render_callback'	=> 'my_acf_block_render_callback',
    'category'			=> 'sharpol',
    'icon'				=> 'visibility',
    'keywords'			=> array( 'zastosowania', 'zabiegi' ),
    )
  );
  acf_register_block(
    array(
    'name'				=> 'appliances',
    'title'				=> __('Urządzenia powiazane'),
    'description'		=> __('Wyświetla produkty powiązane z zabiegiem.'),
    'render_callback'	=> 'my_acf_block_render_callback',
    'category'			=> 'sharpol',
    'icon'				=> 'visibility',
    'keywords'			=> array( 'urządzenia' ),
    )
  );
	}
}


function my_acf_block_render_callback( $block ) {
	
	// convert name ("acf/testimonial") into path friendly slug ("testimonial")
	$slug = str_replace('acf/', '', $block['name']);
	
	// include a template part from within the "template-parts/block" folder
	if( file_exists( get_theme_file_path("/template-parts/blocks/{$slug}.php") ) ) {
		include( get_theme_file_path("/template-parts/blocks/{$slug}.php") );
	}
}