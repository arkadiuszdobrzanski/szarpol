<?php get_header();
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$args = array(
'post_type' => 'blog',
'post_status' => 'publish',
'posts_per_page' => 8,
	'paged'=>$paged
);

$loop = new WP_Query( $args );
//die;
?>
<main class="blog-posts"> 
    <div class="container">
        <h1 class="page-title">Blog</h1>
	<div class="row">

		<div class="col-xl-12 col-lg-12 col-md-9">
			<?php
				if ($loop->have_posts()): ?>
				<div class="fh-blog-grid">
				<?php 
				$i = 0; 
				$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
				while ( $loop->have_posts() ) :
					$i++;
                    $loop->the_post(); ?>

					<?php if ($paged == 1 && $i == 1): ?>
						<article class="<?php echo get_the_category()[0]->slug; ?> big" style="background-image: url('<?php echo get_the_post_thumbnail_url(); ?>')">
							<div class="article-meta">
									<span class="category">
										<?php 
										foreach(get_the_category() as $cat):
											if (get_field('format_wyswietlania', $cat) == 'nazwa_nad_postem'):
												echo $cat->name.'&nbsp;&nbsp;';
											endif; 
										endforeach; ?>
									</span>
								<div class="line">
									<hr>
								</div>
								<?php echo (!empty(get_the_date()) ? '<span class="date">'.get_the_date('d.m.Y').'</span>' : '') ?>
							</div>
							<a href="<?php the_permalink(); ?>">
								<div class="title">
									<?php the_title('<h2>', '</h2>'); ?>
								</div>
							</a>
						</article>

					<?php else: ?>
					<?php $category = get_the_category(); 
					//var_dump($category[0]);
					
					?>
						<article class="<?php echo get_the_category()[0]->slug; ?>">

							<?php get_field('format_wyswietlania', get_the_category()[0]); ?>

							<div class="article-meta">
								<span class="category">
								<?php 
									foreach(get_the_category() as $cat):
										if (get_field('format_wyswietlania', $cat) == 'nazwa_nad_postem'):
											echo $cat->name.'&nbsp;&nbsp;';
										endif; 
									endforeach; ?>
								</span>
								<div class="line">
									<hr>
								</div>
								<?php echo (!empty(get_the_date()) ? '<span class="date">'.get_the_date('d.m.Y').'</span>' : '') ?>
							</div>
							<a href="<?php the_permalink(); ?>">
								<?php the_post_thumbnail(); ?>
								<?php if (get_field('format_wyswietlania', get_the_category()[0]) == 'pasek_na_poscie'): ?>
									<hr style="border-color: <?php echo get_field('kolor_paska', get_the_category()[0]); ?>">
								<?php else: ?>
									<hr>
								<?php endif; ?>
								<div class="title">
									<?php the_title('<h2>', '</h2>'); ?>
								</div>
								<div class="read-more">
									Czytaj dalej
								</div>
							</a>
						</article>
				<?php endif; ?>
					
				<?php endwhile; ?>
				</div>
				<?php
							$GLOBALS['wp_query']->max_num_pages = $loop->max_num_pages;

				// Paginacja
				the_posts_pagination( array(
					'mid_size'  => 4,
					'prev_text' => __( 'Nowsze', 'textdomain' ),
					'next_text' => __( 'Starsze', 'textdomain' ),
					'screen_reader_text' => __( ' ' )
				) );

			endif; ?>
		</div>
	</div>
</main>

<?php get_footer();