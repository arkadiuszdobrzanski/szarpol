<?php get_header(); ?>

	<main class="single-page">
		<article class="single-page-content">
			<?php while ( have_posts() ) :
				the_post();

				the_content();

			endwhile; ?>
		</article>
	</main>

<?php get_footer();