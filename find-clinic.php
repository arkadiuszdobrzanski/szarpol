<?php /* Template Name: Znajdź klinikę */ ?>
<?php get_header(); ?>
<script>
	function getLocation() {
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(showPosition);
			console.log("asd");
		} else {
			console.log("ups");
		}
	}

	function showPosition(position) {
		request = $.ajax({
			//uderzam do customowego routingu
			url: `https://maps.googleapis.com/maps/api/geocode/json?latlng=${position.coords.latitude},${position.coords.longitude}&result_type=administrative_area_level_1&key=AIzaSyB832_bHfRQLmwHuIxiTy8K-5_REW1AAlY`,
			// url: "/wp-json/jobs/v1/jobsMainSearch",
			type: "GET",
			fail: function() {},
			success: function(data) {
				console.log(data.results[0].formatted_address);
			},
		});
		// alert(position.coords.latitude + " " + position.coords.longitude);
	}

	getLocation();
</script>
<?php while (have_posts()) :
	the_post(); ?>
	<main onload="getLocation();" class="find-clinic-main-content" id="particles-js-2">
		<?php the_title('<h1 class="page-title">', '</h1>'); ?>
		<div class="row">
			<aside class="col-lg-3 col-md-4">
				<form action="<?php global $wp;
											echo home_url($wp->request); ?>" method="get">
					<label>
						<span>Województwo</span>
						<select name="wojewodztwo">
							<option value="">Wybierz województwo</option>
							<option value="dolnoslaskie" <?php echo (($_GET['wojewodztwo'] == 'dolnoslaskie') ? 'selected' : '') ?>>Dolnośląskie</option>
							<option value="kujawsko-pomorskie" <?php echo (($_GET['wojewodztwo'] == 'kujawsko-pomorskie') ? 'selected' : '') ?>>Kujawko-pomorskie</option>
							<option value="lubelskie" <?php echo (($_GET['wojewodztwo'] == 'lubelskie') ? 'selected' : '') ?>>Lubelskie</option>
							<option value="lubuskie" <?php echo (($_GET['wojewodztwo'] == 'lubuskie') ? 'selected' : '') ?>>Lubuskie</option>
							<option value="lodzkie" <?php echo (($_GET['wojewodztwo'] == 'lodzkie') ? 'selected' : '') ?>>Łódzkie</option>
							<option value="malopolskie" <?php echo (($_GET['wojewodztwo'] == 'malopolskie') ? 'selected' : '') ?>>Małopolskie</option>
							<option value="mazowieckie" <?php echo (($_GET['wojewodztwo'] == 'mazowieckie') ? 'selected' : '') ?>>Mazowieckie</option>
							<option value="opolskie" <?php echo (($_GET['wojewodztwo'] == 'opolskie') ? 'selected' : '') ?>>Opolskie</option>
							<option value="podlaskie" <?php echo (($_GET['wojewodztwo'] == 'podlaskie') ? 'selected' : '') ?>>Podlaskie</option>
							<option value="pomorskie" <?php echo (($_GET['wojewodztwo'] == 'pomorskie') ? 'selected' : '') ?>>Pomorskie</option>
							<option value="podkarpackie" <?php echo (($_GET['wojewodztwo'] == 'podkarpackie') ? 'selected' : '') ?>>Podkarpackie</option>
							<option value="slaskie" <?php echo (($_GET['wojewodztwo'] == 'slaskie') ? 'selected' : '') ?>>Śląskie</option>
							<option value="swietokrzyskie" <?php echo (($_GET['wojewodztwo'] == 'swietokrzyskie') ? 'selected' : '') ?>>Świętokrzyskie</option>
							<option value="warminsko-mazurskie" <?php echo (($_GET['wojewodztwo'] == 'warminsko-mazurskie') ? 'selected' : '') ?>>Warmińsko-mazurskie</option>
							<option value="wielkopolskie" <?php echo (($_GET['wojewodztwo'] == 'wielkopolskie') ? 'selected' : '') ?>>Wielkopolskie</option>
							<option value="zachodniopomorskie" <?php echo (($_GET['wojewodztwo'] == 'zachodniopomorskie') ? 'selected' : '') ?>>Zachodniopomorskie</option>
						</select>
						</select>
					</label>

					<label>
						<span>Zabieg</span>
						<select name="zabiegi">
							<option value="">Wybierz zabieg</option>
							<?php
							$loop = new WP_Query(array(
								'post_type' => 'treatments',
								'ignore_sticky_posts' => 1,
								'posts_per_page' => -1,
								'post_status' => array('publish'),
								'orderby' => 'title',
								'order' => 'ASC',
							));
							if ($loop->have_posts()) :
								while ($loop->have_posts()) : $loop->the_post(); ?>
									<option value="<?php the_id(); ?>" <?php echo (($_GET['zabiegi'] == get_the_id()) ? 'selected' : ''); ?>><?php the_title(); ?></option>
							<?php endwhile;
							endif;
							wp_reset_postdata(); ?>
						</select>
					</label>


					<label>
						<span>Urządzenie</span>
						<select name="urzadzenia">
							<option value="">Wybierz urządzenie</option>
							<?php
							$loop = new WP_Query(array(
								'post_type' => 'appliance',
								'ignore_sticky_posts' => 1,
								'posts_per_page' => -1,
								'post_status' => array('publish'),
								'orderby' => 'title',
								'order' => 'ASC'
							));
							if ($loop->have_posts()) :
								while ($loop->have_posts()) : $loop->the_post(); ?>
									<option value="<?php the_id(); ?>" <?php echo (($_GET['urzadzenia'] == get_the_id()) ? 'selected' : ''); ?>><?php the_title(); ?></option>
							<?php endwhile;
							endif;
							wp_reset_postdata(); ?>
						</select>
					</label>


					<button type="submit">Szukaj</button>
				</form>
				<?php

				if (!empty($_GET['wojewodztwo'])) :
					$wojewodztwo_query = array(
						'key'     => 'wojewodztwo',
						'value'   => $_GET['wojewodztwo'],
						'compare' => '='
					);
				endif;
				if (!empty($_GET['zabiegi'])) :
					$zabiegi_query = array(
						'key'     => 'zabiegi',
						'value'   => '"' . $_GET['zabiegi'] . '"',
						'compare' => 'LIKE',
					);
				endif;
				if (!empty($_GET['urzadzenia'])) :
					$urzadzenia_query = array(
						'key'     => 'urzadzenia',
						'value'   => '"' . $_GET['urzadzenia'] . '"',
						'compare' => 'LIKE',
					);
				endif;


				$args = array(
					'role' => 'Subscriber',
					'meta_query' => array(
						'relation' => 'AND',
						$wojewodztwo_query,
						$zabiegi_query,
						$urzadzenia_query
					)
				);

				$wp_query = new WP_User_Query($args);

				// Get the results
				$clinics = $wp_query->get_results();

				if ($clinics && ((!empty($_GET['urzadzenia'])) || (!empty($_GET['zabiegi'])) || (!empty($_GET['wojewodztwo'])))) : ?>
					<div class="clinics-resoults row">
						<?php foreach ($clinics as $clinic) : ?>
							<figure class="col-md-12 col-sm-6">
								<a href="<?php echo esc_url(add_query_arg('klinika', $clinic->ID)); ?>">
									<h3><?php the_field('nazwa_kliniki', 'user_' . $clinic->ID) ?></h3>
								</a>
								<?php if (!empty(get_field('adres', 'user_' . $clinic->ID))) : ?>
									<a href="">
										<p class="adres"><?php the_field('adres', 'user_' . $clinic->ID) ?></p>
									</a>
								<?php endif; ?>
								<?php if (!empty(get_field('telefon', 'user_' . $clinic->ID))) : ?>
									<p class="telefon"><?php the_field('telefon', 'user_' . $clinic->ID) ?></p>
								<?php endif; ?>
								<?php if (!empty(get_field('strona_www', 'user_' . $clinic->ID))) : ?>
									<p class="link"><a href="<?php the_field('strona_www', 'user_' . $clinic->ID) ?>" target="_blank"><?php the_field('strona_www', 'user_' . $clinic->ID) ?></a></p>
								<?php endif; ?>
							</figure>
						<?php endforeach; ?>
					</div>
				<?php elseif (isset($_GET['klinika']) && empty($_GET['urzadzenia']) && empty($_GET['zabiegi']) && empty($_GET['wojewodztwo'])) :  ?>
					<div class="clinics-resoults row">
						<figure class="col-md-12 col-sm-6">
							<a href="<?php echo esc_url(add_query_arg('klinika', $_GET['klinika'])); ?>">
								<h3><?php the_field('nazwa_kliniki', 'user_' . $_GET['klinika']) ?></h3>
							</a>
							<?php if (!empty(get_field('adres', 'user_' . $_GET['klinika']))) : ?>
								<p class="adres"><a href="<?php echo esc_url(add_query_arg('klinika', $_GET['klinika'])); ?>"><?php the_field('adres', 'user_' . $_GET['klinika']) ?></a></p>
							<?php endif; ?>
							<?php if (!empty(get_field('telefon', 'user_' . $_GET['klinika']))) : ?>
								<p class="telefon"><?php the_field('telefon', 'user_' . $_GET['klinika']) ?></p>
							<?php endif; ?>
							<?php if (!empty(get_field('strona_www', 'user_' . $_GET['klinika']))) : ?>
								<p class="link"><a href="<?php the_field('strona_www', 'user_' . $_GET['klinika']) ?>" target="_blank"><?php the_field('strona_www', 'user_' . $_GET['klinika']) ?></a></p>
							<?php endif; ?>
						</figure>
					</div>
				<?php else : ?>
					<div class="clinics-noresoults">
						<p>Brak wyników spełniających wybrane kryteria.</p>
					</div>
				<?php endif; ?>


			</aside>
			<div class="col-lg-9 col-md-8">
				<iframe src="
				<?php if (isset($_GET['klinika']) && !empty(get_field('url_google_mapy', 'user_' . $_GET['klinika']))) {
					the_field('url_google_mapy', 'user_' . $_GET['klinika']);
				} else {
					echo 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2632530.7114659417!2d16.55382884192401!3d51.900455392988135!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47009964a4640bbb%3A0x97573ca49cc55ea!2sPolska!5e0!3m2!1spl!2spl!4v1559297263023!5m2!1spl!2spl';
				};
				?>
			" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
		</div>
	</main>
<?php endwhile; ?>
<?php get_footer();
