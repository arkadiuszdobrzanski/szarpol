<?php /* Template Name: Zabiegi */ ?>
<?php get_header(); ?>

<main class="appliances-main-content"> 
    <section class="appliances-header" style="background-image: url('<?php the_field('tlo'); ?>');">
        <div class="wrapper">
        <?php while ( have_posts() ) :
            the_post();
            the_title('<h1 class="page-title">', '</h1>');
            the_content();
        endwhile; ?>
        </div>
    </section>

    <div class="container">

    <div class="row">
        <div class="fh-gallery">
            <ul class="grid">
            <?php 
                $query = new WP_Query( array( 
                    'post_type' => 'treatments',
                    'post_status' => 'publish',
                    'posts_per_page' => -1,
                    'nopaging' => true,
                    'orderby' => 'title',
                    'order' => 'asc',
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'medicine',
                            'field' => 'term_id',
                            'terms' => wp_get_post_terms( $post->ID, 'medicine', array("fields" => "ids") )[0],
                        ))
                )); 
            if ( $query->have_posts()) : 
                while ( $query->have_posts() ) : $query->the_post(); ?>
                    <li class="mix">
                        <a href="<?php the_permalink(); ?>">
                            <figure class="treatment-wrapper">
                                <?php the_post_thumbnail('thumbnail'); ?>
                                <h2><?php the_title(); ?></h2>
                            </figure>
                        </a>
                    </li>
                <?php endwhile;
            endif;
            wp_reset_postdata(); ?>
            </ul>
        </div>
    </div>
    </div>
</main>

<?php
get_footer();
