<?php
/* Template Name: Wyszukiwarka - nowa */
get_header();

$query = $_GET['keyword'];
$blog = array(
    'post_type' => 'blog',
    'posts_per_page' => 6,
    'post_status' => 'publish',
    's' => $query
);

$news = array(
    'post_type' => 'aktualnosci',
    'posts_per_page' => 6,
    'post_status' => 'publish',
    's' => $query,
    'orderby'   => 'date',
    'order' => 'DESC'
);

$products = array(
    'post_type' => 'appliance',
    'posts_per_page' => 8,
    'post_status' => 'publish',
    's' => $query,
    'orderby'   => 'date',
    'order' => 'DESC'
);

$wp_blog = new WP_Query($blog);
$wp_news = new WP_Query($news);
$wp_products = new WP_Query($products);

?>
    <input type="hidden" name="keyword" id="keyword" class="keyword" value="<?php echo $query; ?>">
    <div class="search-container">
        <div class="container">
            <div class="search-title-wrapper">
                <h2 class="search-title">Wyniki wyszukiwania dla "<?php echo $query ?>"</h2>
            </div>
            <div class="products-wrapper">
                <div class="appliances-container">
                    <h2 class="products-title">Produkty</h2>
                    <div class="row">
                        <?php if ($wp_products->posts) {
                            foreach ($wp_products->posts as $product) { ?>
                                <div class="col-3">
                                    <a href="<?php echo get_the_permalink($product); ?>">
                                        <div class="card">
                                            <div class="card-img-top">
                                                <img src="<?php echo get_the_post_thumbnail_url($product); ?>" alt="">
                                            </div>
                                            <div class="card-body">
                                                <h2 class="card-title"><?php echo get_the_title($product); ?></h2>
                                                <p class="card-text"><?php echo get_field('sub_name', $product); ?></p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            <?php }
                        } else { ?>
                            <p class="card-error">Brak wyników wyszukiwania</p>
                        <?php } ?>
                    </div>
                </div>
                <div class="nav-links">
                    <a class="page-numbers n-products" href="#">1</a>
                    <a class="page-numbers n-products" href="#">2</a>
                    <a class="page-numbers n-products" href="#">3</a>
                    <a class="page-numbers n-products" href="#">4</a>
                    <a class="page-numbers n-products" href="#">5</a>
                    <a class="next page-numbers n-products" href="/produkty/urzadzenia-sprzet-do-medycyny-estetycznej/"">Więcej</a>
                </div>
            </div>


            <div class="blog-wrapper">
                <div class="blog-container">
                    <h2 class="blog-title">Blog</h2>
                    <div class="row-alt">
                        <?php if ($wp_blog->posts) {
                            foreach ($wp_blog->posts as $blog) { ?>
                                <div class="col-3">
                                    <a href="<?php echo get_the_permalink($blog); ?>">
                                        <div class="card">
                                            <div class="card-img-top">
                                                <img src="<?php echo get_the_post_thumbnail_url($blog); ?>" alt="">
                                            </div>
                                            <div class="card-body">
                                                <h2 class="card-title"><?php echo get_the_title($blog); ?></h2>
                                                <p class="card-text"><?php echo get_the_excerpt($blog); ?></p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            <?php }
                        } else { ?>
                            <p class="card-error">Brak wyników wyszukiwania</p>
                        <?php } ?>
                    </div>
                </div>
                <div class="nav-links">
                    <a class="page-numbers n-blog" href="#">1</a>
                    <a class="page-numbers n-blog" href="#">2</a>
                    <a class="page-numbers n-blog" href="#">3</a>
                    <a class="page-numbers n-blog" href="#">4</a>
                    <a class="page-numbers n-blog" href="#">5</a>
                    <a class="next page-numbers n-blog" href="#">Starsze</a>
                </div>
            </div>


            <div class="news-wrapper">
                <div class="news-container">
                    <h2 class="news-title">Aktualności</h2>
                    <div class="row-alt">
                        <?php if ($wp_news->posts) {
                            foreach ($wp_news->posts as $news) { ?>
                                <div class="col-3">
                                    <a href="<?php echo get_the_permalink($news); ?>">
                                        <div class="card">
                                            <div class="card-img-top">
                                                <img src="<?php echo get_the_post_thumbnail_url($news); ?>" alt="">
                                            </div>
                                            <div class="card-body">
                                                <h2 class="card-title"><?php echo get_the_title($news); ?></h2>
                                                <p class="card-text"><?php echo get_the_excerpt($news); ?></p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            <?php }
                        } else { ?>
                            <p class="card-error">Brak wyników wyszukiwania</p>
                        <?php }  ?>
                    </div>
                </div>
                <div class="nav-links">
                    <a class="page-numbers n-news" href="#">1</a>
                    <a class="page-numbers n-news" href="#">2</a>
                    <a class="page-numbers n-news" href="#">3</a>
                    <a class="page-numbers n-news" href="#">4</a>
                    <a class="page-numbers n-news" href="#">5</a>
                    <a class="next page-numbers n-news" href="#">Starsze</a>
                </div>
            </div>
        </div>
    </div>

    <script src="<?php echo get_template_directory_uri();?>/js/search.js"></script>
<?php
get_footer();
?>