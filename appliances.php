<?php /* Template Name: Produkty */ ?>
<?php get_header(); ?>
<style>

	.fh-gallery > ul > li a:hover figure.product-wrapper {
   
    z-index: 3 !important;
}
	
</style>
<main class="appliances-main-content"> 
    <section class="appliances-header" style="background-image: url('<?php the_field('tlo'); ?>');" id="particles-js-2">
        <div class="wrapper">
        <?php while ( have_posts() ) :
            the_post();
            the_title('<h1 class="page-title">', '</h1>');
            the_content();
        endwhile; ?>
        </div>
    </section>

    <div class="container">

    <div class="row">
        <aside class="fh-filter col-xl-2 col-lg-3 col-md-4">
			<form>
				<div class="fh-filter-block">
                    <h4>Nowość</h4>
                    <div class="controls fh-filter-content">
                        <label class="checkbox-label" for="all">
                            <button type="button" class="control" data-filter="all" id="all" style="padding-bottom: 7px;"></button>
                            <span>Pokaż wszystko</span>
                        </label>
                   
                  
                            <label class="checkbox-label" for="nowosc">
                                <button type="button" class="control" data-toggle=".nowosc" id="nowosc" style="padding-bottom: 7px;"></button>
                                <span>Pokaż nowości</span>
                            </label>
                  
				</div>
				<div class="fh-filter-block" style="margin-top: 9%;">
                    <h4>Zabiegi</h4>
                    <div class="controls fh-filter-content">
                        <label class="checkbox-label" for="all">
                            <button type="button" class="control" data-filter="all" id="all" style="padding-bottom: 7px;"></button>
                            <span>Pokaż wszystko</span>
                        </label>
                    <?php 
                    $query = new WP_Query( array( 
                        'post_type' => 'treatments',
                        'post_status' => 'publish',
                        'posts_per_page' => -1,
                       
                        'tax_query' => array(
                            array(
                                'taxonomy' => 'medicine',
                                'field' => 'term_id',
                                'terms' => wp_get_post_terms( $post->ID, 'medicine', array("fields" => "ids") )[0],
                            ))
                    )); 
                    if ( $query->have_posts()) : 
                        while ( $query->have_posts() ) : $query->the_post(); ?>
                            <label class="checkbox-label" for="zab_<?php the_ID();?>">
                                <button type="button" class="control" data-toggle=".zab_<?php the_ID();?>" id="zab_<?php the_ID();?>" style="padding-bottom: 7px;"></button>
                                <span><?php the_title(); ?></span>
                            </label>
                        <?php endwhile;
                    endif;
                    wp_reset_postdata(); ?>
				</div>
<div class="fh-filter-block" style="margin-top: 9%;">
                    <h4>Producenci</h4>
                    <div class="controls fh-filter-content" >
                        <label class="checkbox-label" for="all">
                            <button type="button" class="control" data-filter="all" id="all" style="padding-bottom: 7px;"></button>
                            <span>Pokaż wszystko</span>
                        </label>
                    <?php 
                    $query = new WP_Query( array( 
                        'post_type' => 'producent',
                        'post_status' => 'publish',
                        'posts_per_page' => -1,
						'orderby' => 'title',
    'order' => 'ASC',
						
                       'tax_query' => array(
                            array(
                                'taxonomy' => 'medicine',
                                'field' => 'term_id',
                                'terms' => wp_get_post_terms( $post->ID, 'medicine', array("fields" => "ids") )[0],
                            ))
                        
                    )); 
					
                    if ( $query->have_posts()) : 
                        while ( $query->have_posts() ) : $query->the_post(); ?>
                            <label class="checkbox-label" for="prod_<?php the_ID();?>">
                                <button type="button" class="control" data-toggle=".prod_<?php the_ID();?>" id="prod_<?php the_ID();?>" style="padding-bottom: 7px;"></button>
                                <span><?php the_title(); ?></span>
                            </label>
                        <?php endwhile;
                    endif;
                    wp_reset_postdata(); ?>
				</div>
			</form>

        </aside>
        <div class="fh-gallery col-xl-10 col-lg-9 col-md-8">
            <ul class="grid">
            <?php 
                $query = new WP_Query( array( 
                    'post_type' => 'appliance',
                    'post_status' => 'publish',
                    'posts_per_page' => -1,
                    'nopaging' => true,
                    
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'medicine',
                            'field' => 'term_id',
                            'terms' => wp_get_post_terms( $post->ID, 'medicine', array("fields" => "ids") )[0],
                        ))
                )); 
            if ( $query->have_posts()) : 
                while ( $query->have_posts() ) : $query->the_post(); ?>
                    <li class="mix <?php if(get_field('appliance-features-group')['nowosc']) { echo 'nowosc '; } echo get_the_title();
                            if( get_field('zabiegi') ): ?>
                                <?php foreach( get_field('zabiegi') as $zabiegi): ?>
                                    <?php
                                    echo 'zab_'.$zabiegi->ID.' '; ?>
                                <?php endforeach; ?>
                            <?php endif; ?>
							   
							   <?php if( get_field('producenci') ): ?>
                               
                                    <?php $producenci = get_field('producenci');
                                    echo 'prod_'.$producenci->ID.' '; ?>
                               
                            <?php endif; ?>

                        "><a href="<?php the_permalink(); ?>">
						
                            <figure class="product-wrapper">
                                <div class="wrapper">
									<?php if(get_field('appliance-features-group')['nowosc']) { ?>
										<img class="nowosc-icon" src="https://shar-pol.pl/wp-content/uploads/2021/02/nowosc.png" style="right:0; top: 0; position: absolute; width: 105px; z-index: 2; transform: translate(10px,-10px);"/>
									<?php } ?>
                                    <div class="details">
                                    <!-- Pokazuj zalety -->
                                        <?php if( get_field('appliance-features-group')['appliance-features'] ): ?>
                                            <ul class="features">
                                            <?php foreach (get_field('appliance-features-group')['appliance-features'] as $zaleta): ?>
                                                <?php echo '<li><span>'.$zaleta['appliance-single-feature'].'</span></li>'; ?>
                                            <?php endforeach; ?>
                                            </ul>
                                        <?php endif; ?>

                                    <!-- Pokazuj zabiegi! -->
                                        <?php if( get_field('zabiegi')): ?>
                                            <h5>Zastosowania</h5>
                                            <ul class="application">
                                            <?php foreach( get_field('zabiegi') as $zabiegi): ?>
                                                <?php echo '<li><span>'.$zabiegi->post_title.'</span></li>'; ?>
                                            <?php endforeach; ?>
                                            </ul>
                                        <?php endif; ?>
                                    </div>
                                    <div class="info">
										<?php if(get_field('producenci')) { $producent = get_field('producenci'); ?>
										<img src="<?php echo get_the_post_thumbnail_url($producent); ?>" style="left:3%; top: 0; position: absolute; width: 65px; z-index: 1;"/>
									<?php } ?>
                                        <?php 
                                        if ( has_post_thumbnail() ):
                                            the_post_thumbnail('thumbnail');
                                        else:
                                            echo '<img src="' . get_bloginfo( 'stylesheet_directory' ) . '/img/placeholder.png" />';
                                        endif; ?>
                                        <h2><?php the_title(); ?></h2>
                                        <span class="subtitle">
                                        <?php 
                                        if (the_field('sub_name')):
                                            the_field('sub_name'); 
                                        else: 
                                            echo '&nbsp;';
                                        endif ?>
                                        </span>
                                    </div>
                                </div>
                            </figure>
                        </a>
                    </li>
                <?php endwhile;
            endif;
            wp_reset_postdata(); ?>
            </ul>
        </div>
    </div>
    </div>
</main>



	


<?php
get_footer();
