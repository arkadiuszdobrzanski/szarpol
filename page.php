<?php get_header(); ?>

<main class="single-page" id="particles-js-2">
	<?php while ( have_posts() ) :
		the_post(); 
			the_title('<h1 class="page-title">', "</h1>")?>
		<article class="single-page-content">
		<?php the_content(); ?>
		</article>

	<?php endwhile; ?>
</main>

<?php get_footer();