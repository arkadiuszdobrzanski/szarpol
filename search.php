<?php get_header(); ?>

<main class="search-results" id="particles-js-2">
<?php if ( have_posts() ) : ?>

	<h1 class="page-title"><?php printf( __( 'Wyniki wyszukiwania: %s' ), get_search_query() ); ?></h1>
	<div class="container-narrow">
	<?php while ( have_posts() ) :
		the_post(); ?>

		<a href="<?php the_permalink(); ?>" class="item">
			<?php the_title('<h3>', '</h3>'); ?>
			<?php the_excerpt(); ?>
		</a>

		
		<?php endwhile;
		the_posts_pagination( array(
			'mid_size'  => 4,
			'prev_text' => __( 'Nowsze', 'textdomain' ),
			'next_text' => __( 'Starsze', 'textdomain' ),
			'screen_reader_text' => __( ' ' )
		) ); ?>
	</div> 
	<?php else : ?>
		<h1 class="page-title">Nie znaleziono</h1>
		<div class="container-narrow">
			<div class="item search-field">
			<p>Niestety szukanej frazy nie znaleziono. Spróbuj wyszukać inne hasło.</p>
			<?php echo get_search_form(); ?>
			</div>
		</div>
	<?php endif;
	?>

</main>

<?php
get_footer();