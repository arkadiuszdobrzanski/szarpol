<?php if (is_home() || is_front_page() || is_page('kontakt')) {?>
 <!-- początek lagów -->
    <main class="contact-main-content" style="<?php echo is_page('Kontakt') ? 'padding-top: 100px' : '' ?>">
        <div class="row">
            <section class="address col-xl-2 col-lg-2 col-md-4 col-sm-5">
                <h2><?php echo get_field('footer_firma', 'option'); ?></h2>
                <p class="adres"><?php echo get_field('footer_adres', 'option'); ?></p>
                <a href="tel:<?php echo get_field('footer_telefon', 'option'); ?>" class="telefon"><?php echo get_field('footer_telefon', 'option'); ?></a>
                <p>Zapytania:</p>
                <p class="mail"><a href="mailto:office@shar-pol.com.pl"><?php echo get_field('footer_mail', 'option'); ?></a></p>
                <p>Zgłoszenia serwisowe:</p>
                <p class="mail"><a href="serwis@shar-pol.com.pl"><?php echo get_field('footer_serwis', 'option'); ?></a></p>
            </section>
            <section class="form col-xl-4 col-lg-4 col-md-8 col-sm-7">
                <?php
                    echo '<h2>Formularz kontaktowy</h2>';
                    echo do_shortcode('[contact-form-7 id="596" title="Formularz kontaktowy"]');
                    ?>
                <!-- <?php var_dump(get_field('formularz')->ID) ?> -->
            </section>
            <section class="map col-lg-6 col-md-12">
                <h2>Znajdź przedstawiciela</h2>
                <div class="row">
                    <div class="col-xl-8 col-lg-8 col-sm-8">
                        <div id="map-poland">
                            <ul class="poland">
                                <li class="pl1" id="dolnoslaskie"><a href="<?php echo esc_url(add_query_arg('przedstawiciel', 'dolnoslaskie')) ?> " rel="nofollow">Dolnośląskie</a></li>
                                <li class="pl2" id="kujawsko-pomorskie"><a href="<?php echo esc_url(add_query_arg('przedstawiciel', 'kujawsko-pomorskie')) ?>" rel="nofollow">Kujawsko-pomorskie</a></li>
                                <li class="pl3" id="lubelskie"><a href="<?php echo esc_url(add_query_arg('przedstawiciel', 'lubelskie')) ?>" rel="nofollow">Lubelskie</a></li>
                                <li class="pl4" id="lubuskie"><a href="<?php echo esc_url(add_query_arg('przedstawiciel', 'lubuskie')) ?>" rel="nofollow">Lubuskie</a></li>
                                <li class="pl5" id="lodzkie"><a href="<?php echo esc_url(add_query_arg('przedstawiciel', 'lodzkie')) ?>" rel="nofollow">Łódzkie</a></li>
                                <li class="pl6" id="malopolskie"><a href="<?php echo esc_url(add_query_arg('przedstawiciel', 'malopolskie')) ?>" rel="nofollow">Małopolskie</a></li>
                                <li class="pl7" id="mazowieckie"><a href="<?php echo esc_url(add_query_arg('przedstawiciel', 'mazowieckie')) ?>" rel="nofollow">Mazowieckie</a></li>
                                <li class="pl8 active" id="opolskie"><a href="<?php echo esc_url(add_query_arg('przedstawiciel', 'opolskie')) ?>" rel="nofollow">Opolskie</a></li>
                                <li class="pl9" id="podkarpackie"><a href="<?php echo esc_url(add_query_arg('przedstawiciel', 'podkarpackie')) ?>" rel="nofollow">Podkarpackie</a></li>
                                <li class="pl10" id="podlaskie"><a href="<?php echo esc_url(add_query_arg('przedstawiciel', 'podlaskie')) ?>" rel="nofollow">Podlaskie</a></li>
                                <li class="pl11" id="pomorskie"><a href="<?php echo esc_url(add_query_arg('przedstawiciel', 'pomorskie')) ?>" rel="nofollow">Pomorskie</a></li>
                                <li class="pl12" id="slaskie"><a href="<?php echo esc_url(add_query_arg('przedstawiciel', 'slaskie')) ?>" rel="nofollow">Śląskie</a></li>
                                <li class="pl13" id="swietokrzyskie"><a href="<?php echo esc_url(add_query_arg('przedstawiciel', 'swietokrzyskie')) ?>" rel="nofollow">Świętokrzyskie</a></li>
                                <li class="pl14" id="warminsko-mazurskie"><a href="<?php echo esc_url(add_query_arg('przedstawiciel', 'warminsko-mazurskie')) ?>" rel="nofollow">Warmińsko-mazurskie</a></li>
                                <li class="pl15" id="wielkopolskie"><a href="<?php echo esc_url(add_query_arg('przedstawiciel', 'wielkopolskie')) ?>" rel="nofollow">Wielkopolskie</a></li>
                                <li class="pl16" id="zachodniopomorskie"><a href="<?php echo esc_url(add_query_arg('przedstawiciel', 'zachodniopomorskie')) ?>" rel="nofollow">Zachodniopomorskie</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-12 col-sm-4 row map-content">
                        <?php
                        if (empty($_GET['przedstawiciel'])) : ?>
                            <p>Wybierz pożądane województwo na mapie obok,<br>
                                aby zobaczyć dane kontaktowe do przedstawicieli Shar-Pol.</p>
                            <?php else :

                            $loop = new WP_Query(array(
                                'post_type' => 'przedstawiciele',
                                'ignore_sticky_posts' => 1,
                                'posts_per_page' => -1,
                                'post_status' => array('publish'),
                                'meta_query' => array(
                                    array(
                                        'key'     => 'wojewodztwo',
                                        'value'   => '"' . $_GET['przedstawiciel'] . '"',
                                        'compare' => 'LIKE',
                                    ),
                                ),

                            ));
                            if ($loop->have_posts()) :
                                while ($loop->have_posts()) : $loop->the_post(); ?>
                                    <figure class="trader col-xxl-6 col-xl-6 col-lg-6 col-md-12">
                                        <h3><?php echo wp_get_post_terms(get_the_ID(), 'medicine')[0]->name; ?></h3>
                                        <?php the_title('<strong>', '</strong>'); ?>
                                        <?php the_content(); ?>
                                    </figure>
                        <?php endwhile;
                            endif;
                            wp_reset_postdata();

                        endif; ?>
                    </div>
                </div>
                <small><?php echo get_field('notatka_pod_mapa_kopia', 'option'); ?></small>
            </section>
        </div>
    </main>
    <?php } ?>

<?php if (is_home() || is_front_page() ) { ?>
    <section class="tags-section">
        <h3>Popularne tagi</h3>
        <?php
        $tagi = explode(',', get_field('tagi_tagi', 'option'));
        ?>
            <ul class="tags-list">
                <?php foreach ($tagi as $term) : ?>
                    <a href="/?s=<?php echo $term ?>" target="_blank">
                    <li class="tags-item">
                        <?php echo $term ?>
                    </li>
                </a>
            <?php endforeach; ?>
        </ul>
        

    </section>
<?php } ?>



 <!-- koniec lagów -->


<style>
    .wp-block-image img {
        max-width: 100%;
        height: auto !important;
    }
    .mCustomScrollBox{
        height: auto !important;
    }
</style>
<section class="front-page-partners">
    <h3>Nasi Partnerzy</h3>
    <?php
    $images = get_field('footer_loga', 'option');

    if ($images) : ?>
        <ul>
            <?php foreach ($images as $image) : ?>
                <li>
                    <img src="<?php echo $image ?>" alt="<?php echo $image['alt']; ?>" />
                </li>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>
</section>
<footer class="site-footer" id="particles-js">
    <div class="row">
        <div class="col-lg-2 col-md-3 col-sm-6  contact">
            <?php dynamic_sidebar('contact'); ?>
        </div>
        <div class="col-lg-8 col-md-9 col-sm-6 footer-menu">
            <?php wp_nav_menu(array('theme_location' => 'footer')); ?>
        </div>
        <div class="col-lg-2 footer-map">
            <a href="<?php the_permalink(get_page_by_path('kontakt')) ?>" title="Znajdź przedstawiciela">
                <img src="<?php echo get_template_directory_uri() . '/img/mapa-stopka.png'; ?>">
            </a>
        </div>
    </div>
</footer>
<div class="debug">
    <?php wp_nav_menu(array('theme_location' => 'bottom')); ?>
    <span class="copyright">
        Partnerzy: <a href="http://inmode.pl/" target="_self">inmode.pl</a> </br> Projekt strony: <a href="https://weblider.eu"><img src="<?php echo get_template_directory_uri() . '/img/logo-weblider@2.png'; ?>"></a>
    </span>
</div>


<link rel="stylesheet" href="https://unpkg.com/flickity@2/dist/flickity.min.css">
<script src="<?php echo get_template_directory_uri(); ?>/js/flick.js"></script>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/flick.css"
<script></script>
<?php wp_footer(); ?>
<script>

</script>

</body>

</html>