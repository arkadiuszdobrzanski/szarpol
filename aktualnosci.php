<?php /* Template Name: Aktualnosci  */ ?>
<?php get_header(); 
// Define page_id
$page_ID = get_the_ID();

?>
<?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$args = array(
    'post_type' => 'aktualnosci',
    'post_status' => 'publish',
    'posts_per_page' => 8,
	'paged'=>$paged
    );

    $loop = new WP_Query( $args );
//die;
?>
<main class="blog-posts">
    <div class="container">
        <h1 class="page-title">Aktualności</h1>
	<div class="row">

		<div class="col-xl-12 col-lg-12 col-md-9">
			<?php
				if ($loop->have_posts()): ?>
				<div class="fh-blog-grid">
				<?php
				$i = 0;
                $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                while ( $loop->have_posts() ) :
					$i++;
					$loop->the_post(); ?>

					<?php if ($paged == 1 && $i == 1): ?>
						<article class="<?php echo get_the_category()[0]->slug; ?> big" style="background-image: url('<?php echo get_the_post_thumbnail_url(); ?>')">
							<div class="article-meta">
									<span class="category">
										<?php
										foreach(get_the_category() as $cat):
											if (get_field('format_wyswietlania', $cat) == 'nazwa_nad_postem'):
												echo $cat->name.'&nbsp;&nbsp;';
											endif;
										endforeach; ?>
									</span>
								<div class="line">
									<hr>
								</div>
								<?php echo (!empty(get_the_date()) ? '<span class="date">'.get_the_date('d.m.Y').'</span>' : '') ?>
							</div>
							<a href="<?php the_permalink(); ?>">
								<div class="title">
									<?php the_title('<h2>', '</h2>'); ?>
								</div>
							</a>
						</article>

					<?php else: ?>
					<?php $category = get_the_category();
					//var_dump($category[0]);

					?>
						<article class="<?php echo get_the_category()[0]->slug; ?>">

							<?php get_field('format_wyswietlania', get_the_category()[0]); ?>

							<div class="article-meta">
								<span class="category">
								<?php
									foreach(get_the_category() as $cat):
										if (get_field('format_wyswietlania', $cat) == 'nazwa_nad_postem'):
											echo $cat->name.'&nbsp;&nbsp;';
										endif;
									endforeach; ?>
								</span>
								<div class="line">
									<hr>
								</div>
								<?php echo (!empty(get_the_date()) ? '<span class="date">'.get_the_date('d.m.Y').'</span>' : '') ?>
							</div>
							<a href="<?php the_permalink(); ?>">
								<?php the_post_thumbnail(); ?>
								<?php if (get_field('format_wyswietlania', get_the_category()[0]) == 'pasek_na_poscie'): ?>
									<hr style="border-color: <?php echo get_field('kolor_paska', get_the_category()[0]); ?>">
								<?php else: ?>
									<hr>
								<?php endif; ?>
								<div class="title">
									<?php the_title('<h2>', '</h2>'); ?>
								</div>
								<div class="read-more">
									Czytaj dalej
								</div>
							</a>
						</article>
				<?php endif; ?>

				<?php endwhile; ?>
				</div>
				<nav class="navigation pagination" role="navigation" aria-label=" ">
		<h2 class="screen-reader-text"> </h2>
		<div class="nav-links"><span aria-current="page" class="news-page-numbers current">1</span>
<a class="news-page-numbers" href="#">2</a>
<a class="news-page-numbers" href="#">3</a>
<a class="news-page-numbers" href="#">4</a>
<a class="news-page-numbers" href="#">5</a>
<a class="news-page-numbers" href="#">6</a>
<a class="next news-page-numbers" href="#">Starsze</a></div>
	</nav>
				<?php		endif; ?>
		</div>
	</div>
</main>
</div>
<?php get_footer();