<?php get_header(); ?>

<main class="front-page">
    <?php while (have_posts()) :
        the_post(); ?>

        <section class="front-page-selector">
            <?php
            if (have_rows('home-page-banner')) :
                while (have_rows('home-page-banner')) : the_row(); ?>
                    <a class="estetyka" href="<?php echo get_sub_field('estetyka')['odnosnik']['url']; ?>" style="background-image: url('<?php echo get_sub_field('estetyka')['tlo']['url']; ?>')">
                        <figure>
                            <h2><?php echo get_sub_field('estetyka')['tytul']; ?></h2>
                            <p><?php echo get_sub_field('estetyka')['zajawka']; ?></p>
                        </figure>
                    </a><a class="chirurgia" href="<?php echo get_sub_field('chirurgia')['odnosnik']['url']; ?>" style="background-image: url('<?php echo get_sub_field('chirurgia')['tlo']['url']; ?>')">
                        <figure>
                            <h2><?php echo get_sub_field('chirurgia')['tytul']; ?></h2>
                            <p><?php echo get_sub_field('chirurgia')['zajawka']; ?></p>
                        </figure>
                    </a>
                <?php endwhile; ?>
            <?php endif; ?>
        </section>

        <!-- Informacje główne o firmie -->
        <section class="about-information row">
            <div class="about-container col-sm-6">
                <div class="about-container flex">
                    <h2>O nas</h2>
                </div>
                <?php echo get_field('o_nas'); ?>
            </div>
            <?php
            $args = array(
                'post_type' => 'reviews',
                'post_status' => 'publish',
                'posts_per_page' => 8,
            );

            $opinie = new WP_Query($args);
            //                var_dump($opinie);
            //                die;

            ?>
            <div class="opinions-container col-sm-6">
                <div class="opinions-container flex">
                    <h2>Opinie</h2>
                    <img class="image" src="<?php echo get_template_directory_uri(); ?>/img/medal.png" alt="">
                </div>
                <div class="opinions-carousel" data-flickity='{ "cellAlign": "center", "contain": true, "prevNextButtons": false, "wrapAround": true, "autoPlay": true, "pauseAutoPlayOnHover": true }'>
                    <?php while (have_rows('opinie')) : the_row(); ?>
                        <p>
                            <?php echo get_sub_field('tresc_opinii'); ?>
                            <br><span style="padding-top: 6px; font-weight: 600;"><?php echo get_sub_field('autor_opinii');  ?> </span>
                        </p>
                    <?php endwhile; ?>
                </div>

            </div>
        </section>
        <section class="front-page-new">
            <div class="row w-100" style="width:100%">
                <h3 class="title-news">Nowości</h3>
                <div class="wrapper w-100" style="width:100%">

                    <?php $nowosci_karuzela = get_field('news_products', 'option');
                    //                         $loop = new WP_Query(array(
                    //                             'post_type' => 'appliance',
                    //                             'ignore_sticky_posts' => 1,
                    //                             'posts_per_page' => 8,
                    //                             'post_status' => array('publish'),
                    //                         ));
                    //
                    if ($nowosci_karuzela) { ?>
                        <div class="carousel-wrapper " data-flickity='{ "cellAlign": "left", "contain": true, "imagesLoaded": true, "setGallerySize": false, "pageDots": false}'>
                            <?php foreach ($nowosci_karuzela as $new) { ?>

                                <div class="col-lg-4 col-md-4 col-sm-12 ">

                                    <a href="<?php echo get_the_permalink($new); ?>" class="review">
                                        <div>
                                            <div class="wrapper card">
                                                <div class="image card-img-top">
                                                    <img class="new-image" src="<?php echo get_the_post_thumbnail_url($new); ?>" alt="">
                                                </div>
                                                <?php if (get_field('format_wyswietlania', get_the_category($new)[0]) == 'pasek_na_poscie') : ?>
                                                    <hr style="border-color: <?php echo get_field('kolor_paska', get_the_category($new)[0]); ?>">
                                                <?php else : ?>
                                                    <hr>
                                                <?php endif; ?>
                                                <div class="title card-body d-flex flex-column">
                                                    <h3><?php echo get_the_title($new); ?></h3>
                                                    <span class="read-more mt-auto">Czytaj dalej </span>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            <?php } ?>
                        </div>
                    <?php }



                    wp_reset_postdata(); ?>
                </div>

            </div>
        </section>
        <section class="about-information row">
            <div class="advantages-container row">
                <h2>Nasze atuty</h2>
                <div class="advantages-container training col-sm-6">
                    <div class="advantages-container flex">
                        <h3>Szkolenia</h3>
                        <img class="image" src="<?php echo get_template_directory_uri();
                                                ?>/img/certyifikat.png" alt="">
                    </div>
                    <img src="" alt="">
                    <?php echo get_field('szkolenia'); ?>

                </div>
                <div class="advantages-container service col-sm-6">
                    <div class="advantages-container flex">
                        <h3>Serwis</h3>
                        <img class="image" src="<?php echo get_template_directory_uri();
                                                ?>/img/dlon.png" alt="">
                    </div>
                    <?php echo get_field('serwis_sekcja'); ?>
                </div>
            </div>
        </section>


        <!--Wpisy na blogu -->
        <section id="<?php echo $id; ?>" class="front-page-blog alignfull">
            <div class="row">
                <div class="heading-row">
                    <h3><a href="<?php echo get_post_type_archive_link('blog'); ?>">Blog</a></h3>
                    <a href="<?php echo get_post_type_archive_link('blog'); ?>" class="read-more">Zobacz więcej</a>
                </div>
            </div>
            <div class="wrapper">
                <div class="carousel-wrapper">
                    <?php
                    $loop = new WP_Query(array(
                        'post_type' => 'blog',
                        'ignore_sticky_posts' => 1,
                        'posts_per_page' => 4,
                        'post_status' => array('publish'),
                    ));
                    if ($loop->have_posts()) :
                        while ($loop->have_posts()) : $loop->the_post(); ?>
                            <a href="<?php the_permalink(); ?>" class="review">
                                <figure>
                                    <div class="wrapper">
                                        <div class="image">
                                            <?php the_post_thumbnail('large'); ?>
                                        </div>
                                        <?php if (get_field('format_wyswietlania', get_the_category()[0]) == 'pasek_na_poscie') : ?>
                                            <hr style="border-color: <?php echo get_field('kolor_paska', get_the_category()[0]); ?>">
                                        <?php else : ?>
                                            <hr>
                                        <?php endif; ?>
                                        <div class="title">
                                            <h3><?php the_title(); ?></h3>
                                            <span class="read-more">Czytaj dalej </span>
                                        </div>
                                    </div>
                                </figure>
                            </a>
                    <?php endwhile;
                    endif;
                    wp_reset_postdata(); ?>
                </div>
            </div>
        </section>

        <!--Karuzela z logotypami -->

    <?php endwhile; ?>
    <style>
        .articles-block.alignfull::before,
        .front-page-aktualnosci.alignfull::before {
            content: 'Aktualności' !important;
            font-size: 7rem;
            display: block;
            font-weight: 900;
            letter-spacing: -3%;
            position: absolute;
            bottom: -24px;
            left: 10px;
            z-index: -1;
            color: #E5E5E5;
        }
    </style>
    <!--Wpisy na blogu -->
    <section id="<?php echo $id; ?>" class="front-page-blog front-page-aktualnosci alignfull">
        <div class="row">
            <div class="heading-row">
                <h3><a href="/aktualnosci">Aktualności</a></h3>
                <a href="/aktualnosci" class="read-more">Zobacz więcej</a>
            </div>
        </div>
        <div class="wrapper">
            <div class="carousel-wrapper">
                <?php
                $loop = new WP_Query(array(
                    'post_type' => 'aktualnosci',
                    'ignore_sticky_posts' => 1,
                    'posts_per_page' => 4,
                    'post_status' => array('publish'),
                ));
                if ($loop->have_posts()) :
                    while ($loop->have_posts()) : $loop->the_post(); ?>
                        <a href="<?php the_permalink(); ?>" class="review">
                            <figure>
                                <div class="wrapper">
                                    <div class="image">
                                        <?php the_post_thumbnail('large'); ?>
                                    </div>
                                    <?php if (get_field('format_wyswietlania', get_the_category()[0]) == 'pasek_na_poscie') : ?>
                                        <hr style="border-color: <?php echo get_field('kolor_paska', get_the_category()[0]); ?>">
                                    <?php else : ?>
                                        <hr>
                                    <?php endif; ?>
                                    <div class="title">
                                        <h3><?php the_title(); ?></h3>
                                        <span class="read-more">Czytaj dalej </span>
                                    </div>
                                </div>
                            </figure>
                        </a>
                <?php endwhile;
                endif;
                wp_reset_postdata(); ?>
            </div>
        </div>
    </section>

</main>

<?php get_footer();
