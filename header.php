<html>

<head>
	<meta charset="<?php bloginfo('charset'); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="profile" href="https://gmpg.org/xfn/11" />
	<?php wp_head(); ?>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-56662842-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());

		gtag('config', 'UA-56662842-1');
	</script>
	<!-- Facebook Pixel Code -->
	<script>
		! function(f, b, e, v, n, t, s) {
			if (f.fbq) return;
			n = f.fbq = function() {
				n.callMethod ?
					n.callMethod.apply(n, arguments) : n.queue.push(arguments)
			};
			if (!f._fbq) f._fbq = n;
			n.push = n;
			n.loaded = !0;
			n.version = '2.0';
			n.queue = [];
			t = b.createElement(e);
			t.async = !0;
			t.src = v;
			s = b.getElementsByTagName(e)[0];
			s.parentNode.insertBefore(t, s)
		}(window, document, 'script',
			'https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '1681267012064651');
		fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1681267012064651&ev=PageView&noscript=1" /></noscript>
	<!-- End Facebook Pixel Code -->

</head>

<body <?php body_class(); ?>>

	<header class="site-header">
		<div class="header-wrapper">
			<?php
			if (has_custom_logo()) :
				the_custom_logo();
			endif;
			?>
			<div class="spacer"></div>
			<?php wp_nav_menu(array('theme_location' => 'primary')); ?>
			<div class="header-search" id="search-trigger">
				<form style="height: 100%" method="get" id="searchform" action="/nowa-wyszukiwarka">
					<input style="height: 100%" type="text" class="field" name="keyword" id="keyword" placeholder="<?php esc_attr_e('Wpisz frazę i wciśnij enter', 'twentyeleve'); ?>" />
				</form>
			</div>
			<a class="header-account" href="<?php the_permalink('681'); ?>"></a>
			<div id="mobile-menu-trigger" class="hamburger hamburger--spring">
				<span class="hamburger-box">
					<span class="hamburger-inner"></span>
				</span>
			</div>
		</div>
	</header>