var delayValue = 400;
function delay(callback, ms) {
    var timer = 0;
    return function () {
        var context = this,
            args = arguments;
        clearTimeout(timer);
        timer = setTimeout(function () {
            callback.apply(context, args);
        }, ms || 0);
    };
}

function submitData(method, searchValue, url, fill, foo) {
    console.log(url + "/" + searchValue + "/" + foo);
    request = $.ajax({
        url: url + "/" + searchValue+ "/" + foo,
        type: method,
        fail: function () {},
        success: function (data) {
            console.log(searchValue);
            $(fill).hide().fadeOut(250).html(data).fadeIn(350);
            //Więc wrzucam 'data' do tej klasy
        },
    });
}

$('.n-products').on('click', function(event) {
    event.preventDefault();
    $('.n-products').removeClass('current');
    $(this).addClass('current')
    submitData('GET', this.text, '/wp-json/paginacja/v1/getProductsSearch', '.appliances-container',  $(".keyword").val());
})

$('.n-news').on('click', function(event) {
    event.preventDefault();
    $('.n-news').removeClass('current');
    $(this).addClass('current')
    submitData('GET', this.text, '/wp-json/paginacja/v1/getNewsSearch', '.news-container', $(".keyword").val());
})

$('.n-blog').on('click', function(event) {
    event.preventDefault();
    $('.n-blog').removeClass('current');
    $(this).addClass('current');
    submitData('GET', this.text, '/wp-json/paginacja/v1/getBlogSearch', '.blog-container', $(".keyword").val());
})