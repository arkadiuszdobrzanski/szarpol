//
// Grid zabiegi i produkty
//

jQuery(document).ready(function($){
	$('.fh-filter-block h4').on('click', function(){
		$(this).toggleClass('closed').siblings('.fh-filter-content').slideToggle(300);
	});

	var containerEl = document.querySelector('.fh-gallery > .grid');

	var mixer = mixitup(containerEl, {
			controls: {
					toggleLogic: 'or'
			}
	});
});

/*  edycja 17.02 */

var ask_form_data = ($(".page-title").text()); 
$(".ask__form__data").find('input[name="your-data"]').val(ask_form_data);

/*  edycja 17.02 */

// Parallax w bloku kontakts
$('.blok-kontaktu, .treatment-intro .image, .front-page-service').parallaxie({
	speed: 0.5,
	offset: 20
});

// Karuzela zdjeć produktu
var $no_items = 3;
if ( $('.galeria-produktu').hasClass( 'alignfull' ) ) {
	$no_items = 5;
}

$('.galeria-produktu > .carousel-wrapper').slick({
  infinite: false,
  slidesToShow: $no_items,
	slidesToScroll: 1,
	dots: true,
	responsive: [
		{
      breakpoint: 1500,
      settings: {
        slidesToShow: 4,
      }
    },
    {
      breakpoint: 1024,
      settings: {
				slidesToShow: 3,
				dots: false,
				arrows: false,
      }
    },
    {
      breakpoint: 700,
      settings: {
        slidesToShow: 2,
				dots: false,
				arrows: false,
      }
    },
    {
      breakpoint: 480,
      settings: {
				slidesToShow: 1,
				arrows: false,
				dots: false,
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});

$('.front-page-blog .carousel-wrapper').slick({
  infinite: false,
  slidesToShow: 4,
	slidesToScroll: 1,
	dots: true,
	arrows: true,
	responsive: [
		{
      breakpoint: 1500,
      settings: {
				slidesToShow: 4,
				arrows: true,
      }
    },
    {
      breakpoint: 1300,
      settings: {
				slidesToShow: 3,
				dots: false,
				arrows: true,
      }
    },
    {
      breakpoint: 1000,
      settings: {
        slidesToShow: 2,
				dots: false,
				arrows: false,
      }
    },
    {
      breakpoint: 550,
      settings: {
				slidesToShow: 1,
				arrows: false,
				dots: false,
      }
    }
  ]
});

$('.zastosowania-urzadzenia .carousel-wrapper, .reviews-block .carousel-wrapper, .articles-block .carousel-wrapper, .urzadzenia .carousel-wrapper').slick({
  infinite: false,
  slidesToShow: 4,
	slidesToScroll: 1,
	dots: false,
	responsive: [
		{
      breakpoint: 1980,
      settings: {
      	slidesToShow: 3,
      }
    },
    {
      breakpoint: 1100,
      settings: {
        slidesToShow: 2,
				arrows: false,
      }
    },
    {
      breakpoint: 580,
      settings: {
				slidesToShow: 1,
				arrows: false,
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});


// Fancybox dla galerii zdjeć produktu
$('[data-fancybox="gallery"], .blocks-gallery-item > figure > a, .wp-block-image > a').fancybox({
	buttons: [
    //"zoom",
    //"share",
    //"slideShow",
    //"fullScreen",
    //"download",
    //"thumbs",
    "close"
  ]
});

$( "#produkt" ).change(function() {
  $( "#filtrowanie_bloga" ).submit();
});

 // CSSMap;
 $("#map-poland").CSSMap({
	size: 850,
	mapStyle: "blue",
  cities: true,
  tooltips: "false",
  responsive: "auto",
  tapOnce: false,
  pins: {
    enable: false
  }
});

$('.front-page-partners ul').slick({
  infinite: true,
  slidesToShow: 7,
	slidesToScroll: 1,
	autoplay: true,
  autoplaySpeed: 2000,
	responsive: [
		{
      breakpoint: 1500,
      settings: {
      	slidesToShow: 5,
      }
		},
		{
      breakpoint: 1300,
      settings: {
				slidesToShow: 4,
				arrows: false,
      }
		},
		{
      breakpoint: 1100,
      settings: {
				slidesToShow: 3,
				arrows: false,
      }
    },
    {
      breakpoint: 800,
      settings: {
        slidesToShow: 2,
				arrows: false,
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});

// Mobile menu
$("#mobile-menu-trigger").click(function(){
	 $( this ).toggleClass( "is-active" );
	 $( '.menu-menu-glowne-container' ).toggleClass( "active" );
});

$(".front-page-boxes > .box").click(function(){
  $(this).toggleClass("active");
});

(function($){
  $(window).on("load",function(){
    
    $("#time-line").mCustomScrollbar({
      theme:"dark-thin",
      axis:"x",
			advanced:{autoExpandHorizontalScroll:true}
    });
    
  });
})(jQuery);

jQuery(document).ready(function($){

  function loadIframe(iframeName, url) {
    var $iframe = $('#' + iframeName);
    if ( $iframe.length ) {
        $iframe.attr('src',url);   
        return false;
    }
    return true;
}

  function get_lat_long() {
    $("#results-list li").click(function(){
      if ($(this).attr("data-url") && $(this).attr("data-url") !== "") {
        loadIframe('google_map_frame', $(this).attr("data-url"));
        $("#results-list li").removeClass('active');
        $(this).addClass('active');
      }
    });
  }

  $(".tooltip").each(function(){
    $(this).tooltipster({
        content: $('<div class="desc"><img src="' + $(this).attr('href') + '"/></div>'),
        interactive: true
    });
  });
  

  $('#find-clinic-form select').on('change', function() {

    if( $('#find-clinic-form select[name=wojewodztwo]').val() === '0' ) {

      $('#find-clinic-form select[name=zabiegi]').prop('selectedIndex',0).prop('disabled', true);
      $('#find-clinic-form select[name=urzadzenia]').prop('selectedIndex',0).prop('disabled', true);
      $( "ul#results-list" ).empty().append('<li><p>Wybierz województwo, aby kontynuować</p></li>');

    } else {

      $('#find-clinic-form select[name=zabiegi]').prop('disabled', false);
      $('#find-clinic-form select[name=urzadzenia]').prop('disabled', false);

      if( ($('#find-clinic-form select[name=zabiegi]').val() !== '0') || ($('#find-clinic-form select[name=urzadzenia]').val() !== '0')) { 

          console.log($("#find-clinic-form select").serialize());
        $.ajax({
          type: 'POST',
          url: '/wp-content/themes/weblider/find-clinic/ajax-kliniki.php',
          data: $("#find-clinic-form select").serialize(),
        })
        .done(function(data){
            console.log(data);
          $( "ul#results-list" ).empty().html(data);
          get_lat_long();
        });

      } else {
          $( "ul#results-list" ).empty().append('<li><p>Aby wyszukać klinikę wybierz zabieg i/lub urządzenie.</p></li>');
      }
    }
});


    $(document).ready(function(){
        setTimeout(function() {
            console.log("asd");

            let foo = $('#map-poland').find('.poland').find('li').find('.m').children().on('click',function(){
                let element = $(this).parent('.m').parent('li').attr('id');
                submitData('GET', element, '/wp-json/mapka/v1/getMapObject', '.map-content');

            })
            console.log(foo);
        },0)

    })

    var delayValue = 400;
    function delay(callback, ms) {
        var timer = 0;
        return function () {
            var context = this,
                args = arguments;
            clearTimeout(timer);
            timer = setTimeout(function () {
                callback.apply(context, args);
            }, ms || 0);
        };
    }

    function submitData(method, searchValue, url, fill) {
        console.log(url + "/" + searchValue);
        request = $.ajax({
            //uderzam do customowego routingu
            url: url + "/" + searchValue,

            type: method,
            fail: function () {},
            success: function (data) {
                //w data jest zwracany cały wygląd HTML wraz z wprowadzonymi danymi
                console.log(searchValue);
                $(fill).hide().fadeOut(250).html(data).fadeIn(350);
                //Więc wrzucam 'data' do tej klasy
            },
        });
    }

    $('.news-page-numbers').on('click', function(event) {
        $('.news-page-numbers').removeClass('current');
        $(this).addClass('current')
        submitData('GET', this.text, '/wp-json/paginacja/v1/getNews', '.fh-blog-grid');
    })


    get_lat_long();

});