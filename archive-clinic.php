<?php /* Template Name: Znajdź klinikę */ ?>
<?php get_header();
$query = $_GET['keyword'];

?>

    <main class="find-clinic-main-content" id="particles-js-2">
        <h1 class="page-title">Znajdź klinikę</h1>

        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-md-7">
                    <form action="<?php global $wp;
                    echo home_url($wp->request); ?>" method="get" class="row" id="find-clinic-form">
                        <label class="col-sm-4">
                            <span>Województwo</span>

                            <select name="wojewodztwo" id="wojewodztwo" autocomplete="off" onchange="clinicHandler()">
                                <option value="0">Wybierz województwo</option>
                                <?php
                                $wojewodztwa = get_terms('wojewodztwo', array(
                                    'hide_empty' => true, 'parent' => 0, 'orderby' => 'name',
                                    'order' => 'ASC'
                                ));
                                if (!empty($wojewodztwa)) :
                                    foreach ($wojewodztwa as $wojewodztwo) {
                                        echo '<option value="' . esc_attr($wojewodztwo->term_id) . '"> ' . esc_html($wojewodztwo->name) . '</option>';
                                    }
                                endif; ?>
                            </select>
                        </label>

                        <label class="col-sm-4">
                            <span>Zabieg</span>
                            <select name="zabiegi" id="zabiegi" onchange="clinicHandler()" autocomplete="off" disabled>
                                <option value="0">Wybierz zabieg</option>
                                <?php
                                $treatments = get_terms(array(
                                    'taxonomy' => 'treatments',
                                    'hide_empty' => false,
                                ));
                                foreach ($treatments as $treatment) {
                                    echo '<option value="' . $treatment->term_id . '">' . $treatment->name . '</option>';
                                }
                                ?>
                            </select>
                        </label>

                        <label class="col-sm-4">
                            <span>Urządzenie</span>
                            <select name="urzadzenia" id="urzadzenia" onchange="clinicHandler()" autocomplete="off" disabled>
                                <option value="0">Dowolne</option>
                                <?php
                                $appliances_sharpol = get_posts(array(
                                    'post_type' => 'appliance',
                                    'ignore_sticky_posts' => 1,
                                    'posts_per_page' => -1,
                                    'post_status' => array('publish'),
                                    'orderby' => 'title',
                                    'order' => 'ASC'
                                ));

                                $appliances_other = get_terms(array(
                                    'taxonomy' => 'appliances_clinics',
                                    'hide_empty' => false,
                                ));

                                $appliances_other_ar = array();
                                foreach ($appliances_other as $appliance_o) {
                                    $appliances_other_ar[] = array(
                                        "id" => 'o' . $appliance_o->term_id,
                                        "name" => $appliance_o->name,
                                    );
                                };

                                foreach ($appliances_other_ar as $appliance) {
                                    echo '<option value="' . $appliance['id'] . '">' . $appliance['name'] . '</option>';
                                }
                                ?>
                                <?php wp_reset_postdata(); ?>
                            </select>
                        </label>

                    </form>

                    <ul id="results-list">
                        <li>
                            <p id="result-clinic-info">Wybierz województwo, aby kontynuować</p>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-7 col-md-5">
                    <iframe class="google-map" id="google_map_frame" src="
					<?php if (isset($_GET['klinika']) && !empty(get_field('url_google_mapy', 'user_' . $_GET['klinika']))) {
                        the_field('url_google_mapy', 'user_' . $_GET['klinika']);
                    } else {
                        echo 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2632530.7114659417!2d16.55382884192401!3d51.900455392988135!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47009964a4640bbb%3A0x97573ca49cc55ea!2sPolska!5e0!3m2!1spl!2spl!4v1559297263023!5m2!1spl!2spl';
                    };
                    ?>
					" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        <script>
            function clinicHandler() {
                let wojewodztwo = $("#wojewodztwo :selected").val();
                let zabieg = $("#zabiegi :selected").val();
                let urzadzenie = $("#urzadzenia :selected").val();
                console.log(`Wojewodztwo: ${wojewodztwo}, Zabieg: ${zabieg}, Urządzenie: ${urzadzenie}`)


                if (wojewodztwo == 0 && zabieg == 0) {
                    document.getElementById('result-clinic-info').innerText = 'Wybierz województwo oraz zabieg który chcesz wykonać'
                } else if (zabieg == 0 && urzadzenie == 0) {
                    document.getElementById('result-clinic-info').innerText = 'Wybierz zabieg lub urządzenie z listy'
                }
            }
        </script>

    </main>
    <script>



        function getLocation() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition);
            } else {
            }
        }

        const voivodeshipNames = {
            'Dolnośląskie': 18,
            'Kujawsko-Pomorskie': 66,
            'Lubelskie': 74,
            'Lubuskie': 69,
            'Łódzkie': 77,
            'Małopolskie': 76,
            'Mazowieckie': 19,
            'Opolskie': 73,
            'Podkarpackie': 75,
            'Podlaskie': 78,
            'Pomorskie': 67,
            'Śląskie': 17,
            'Świętokrzyskie': 68,
            'Warmińsko-Mazurskie': 70,
            'Wielkopolskie': 72,
            'Zachodniopomorskie': 71
        }

        function getKeyByValue(object, value) {
            return Object.keys(object).find(key => object[key] === value);
        }

        function showPosition(position) {
            request = $.ajax({
                //uderzam do customowego routingu
                url: `https://maps.googleapis.com/maps/api/geocode/json?latlng=${position.coords.latitude},${position.coords.longitude}&result_type=administrative_area_level_1&key=AIzaSyB832_bHfRQLmwHuIxiTy8K-5_REW1AAlY`,
                // url: "/wp-json/jobs/v1/jobsMainSearch",
                type: "GET",
                fail: function() {},
                success: function(data) {
                    const voivodeship = data.results[0].address_components[0].long_name;
                    // console.log(voivodeship);
                    // console.log(getKeyByValue(voivodeshipNames, voivodeship));
                    console.log($("#wojewodztwo").val(voivodeshipNames[voivodeship]).change());
                    // console.log(" " + voivodeship);
                    $("#zabiegi").prop("disabled", false);
                    $("#urzadzenia").prop("disabled", false);
                    const keyword = "<?php echo $query;?>";
                    if(keyword == "Cristal"){
                        $("#urzadzenia").val("o104").change();
                        $("#urzadzenia").prop("disabled", false);
                        if( $('#find-clinic-form select[name=wojewodztwo]').val() === '0' ) {
                            $('#find-clinic-form select[name=zabiegi]').prop('selectedIndex',0).prop('disabled', true);
                            $('#find-clinic-form select[name=urzadzenia]').prop('selectedIndex',0).prop('disabled', true);
                            $( "ul#results-list" ).empty().append('<li><p>Wybierz województwo, aby kontynuować</p></li>');
                        } else {
                            $('#find-clinic-form select[name=zabiegi]').prop('disabled', false);
                            $('#find-clinic-form select[name=urzadzenia]').prop('disabled', false);
                            if( ($('#find-clinic-form select[name=zabiegi]').val() !== '0') || ($('#find-clinic-form select[name=urzadzenia]').val() !== '0')) {
                                console.log($("#find-clinic-form select").serialize());
                                $.ajax({
                                    type: 'POST',
                                    url: '/wp-content/themes/weblider/find-clinic/ajax-kliniki.php',
                                    data: $("#find-clinic-form select").serialize(),
                                })
                                    .done(function(data){
                                        console.log(data);
                                        $( "ul#results-list" ).empty().html(data);
                                        get_lat_long();
                                    });
                            } else {
                                $( "ul#results-list" ).empty().append('<li><p>Aby wyszukać klinikę wybierz zabieg i/lub urządzenie.</p></li>');
                            }
                        }
                    }
                },
            });
            // alert(position.coords.latitude + " " + position.coords.longitude);
        }

        getLocation();
    </script>
<?php get_footer();
