<?php get_header(); ?>
<main class="single-appliance">

<?php while ( have_posts() ) :
the_post(); ?>

<!-- Sekcja nagłówka -->
<?php the_title('<h1 class="page-title">', '</h1>'); ?>

<nav class="single-top-navigation">
	<a href="javascript:history.back()">&nbsp;</a> Powrót do wyboru produktów
</nav>

<!-- Opis produktu -->
<section class="appliance-intro">
	<div class="row">
		<div class="features col-lg-3">
			<h2>Typ produktu: <strong><?php the_field('sub_name'); ?></strong></h2>
			<?php
			$appliance_features = get_field('appliance-features-group')['appliance-features'];
			if (!empty($appliance_features)):
			echo '<ul>';
			foreach ($appliance_features as $key) {
				echo '<li>'.$key['appliance-single-feature'].'</li>';
			}
			echo '</ul>';
			endif; 
			?>
		</div>
		<div class="description col-lg-9">
			<div class="row">
			<?php 
			if (has_post_thumbnail()): ?>
				<div class="col-md-6"> 
					<?php echo get_field('appliance-description-group')['appliance-description'] ?>
				</div>
				<div class="col-md-6">
					<a href="<?php the_post_thumbnail_url(); ?>" data-fancybox="gallery"  data-options='{"caption" : "<?php the_title(); ?>"}'>
						<?php the_post_thumbnail('large'); ?>
					</a>
				</div>
			<?php else: ?> 
				<div class="col-md-12"> 
					<?php echo get_field('appliance-description-group')['appliance-description'] ?>
				</div>
			<?php endif; ?>
			</div>
		</div>
	</div>
</section>

<div class="content">
	<?php the_content(); ?>
</div>

<?php endwhile; ?>

</main>
<?php get_footer();